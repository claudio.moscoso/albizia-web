<%
	javax.servlet.ServletContext context = application.getContext("/dalea-web");

	javax.servlet.RequestDispatcher rdHeader = context.getRequestDispatcher("/WEB-INF/jsp/common/header2.jsp");
	javax.servlet.RequestDispatcher rdMenu = context.getRequestDispatcher("/WEB-INF/jsp/common/menu2.jsp");
	javax.servlet.RequestDispatcher rdFooter = context.getRequestDispatcher("/WEB-INF/jsp/common/footer2.jsp");
	
	rdHeader.include(request, response);
	rdMenu.include(request, response);
	getServletContext().getRequestDispatcher("/WEB-INF/jsp/period/period-duplicate.jsp").include(request, response);
	
	rdFooter.include(request, response);
%>
