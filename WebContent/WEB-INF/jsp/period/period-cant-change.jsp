<%
String periodName = (String) request.getAttribute("PeriodName");
String statusName = (String) request.getAttribute("StatusName");
%>
<div class="page-header">
	<h1>Modificaci�n de datos del per�odo</h1>
</div>
 
<div class="row">
<span class="well">El per�odo "<%=periodName%>" no puede ser cambiado por que est� en estado "<%=statusName%>"</span>
<br>
<br>
<a class="btn btn-default"
	href="${pageContext.request.contextPath}/servlet/manager/period/PeriodManager">Volver</a>
</div>