<%@page import="cl.buildersoft.framework.util.BSWeb"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript"
	src="${applicationScope['ALBIZIA_CONTEXT']}/js/employee/hours-worked.js?<%=BSWeb.randomString()%>"></script>

<div class="page-header">
	<h1>Horas trabajadas</h1>
</div>

<form class="form-horizontal"
	method="post" id="form">

	<div class="row">
		<label class="col-sm-2">Periodo:</label>
		<c:out value="${requestScope.Period}" />
		<input type="hidden" name="PeriodId" value="${requestScope.PeriodId}">
	</div>
</form>

<div class="row">
	<table id="DataTable"
		class="table table-striped table-bordered table-hover table-condensed table-responsive hover compact">
		<thead>
			<tr>
				<th>Id</th>
				<th>Rut</th>
				<th>Nombre</th>
				<th>Horas</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.Employees}" var="row">
				<tr style="cursor: pointer;">
					<td>${row.employeeId}</td>
					<td>${row.rut}</td>
					<td>${row.name}</td>
					<td>${row.amount}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<button class="btn btn-default" id="changeHoursButton" disabled onclick="javascript:doAction()">Cambiar horas</button>

