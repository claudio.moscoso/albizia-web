<%@page import="cl.buildersoft.framework.util.BSWeb"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="cl.buildersoft.dalea.bean.Health"%>
<%@page import="cl.buildersoft.dalea.bean.PFM"%>
<%@page import="cl.buildersoft.framework.type.Currency"%>
<%@page import="cl.buildersoft.dalea.bean.Employee"%>
<%@page import="cl.buildersoft.dalea.bean.Agreement"%>
<%@page
	import="cl.buildersoft.albizia.business.beans.FamilyAssignmentStretch"%>
<%@page import="cl.buildersoft.albizia.business.beans.ExBoxSystem"%>
<%@page import="cl.buildersoft.albizia.business.beans.RagreementAPV"%>

<%@page import="cl.buildersoft.albizia.business.beans.APV"%>
<%@page import="java.util.List"%>
<%@page import="cl.buildersoft.framework.util.crud.BSField"%>
<%@page import="cl.buildersoft.framework.util.crud.BSTableConfig"%>

<%
	//	BSTableConfig table = (BSTableConfig) session.getAttribute("BSTable");
	//	BSField[] fields = table.getFields();

//	List<APV> listadoApv = (List<APV>) request.getAttribute("listadoApv");
	//	List<Currency> listadoCurrency = (List<Currency>) request.getAttribute("listadoCurrency");
//	List<RagreementAPV> listadoApvEmp = (List<RagreementAPV>) request.getAttribute("listadoApvEmp");
	List<PFM> listadoAfp = (List<PFM>) request.getAttribute("listadoAfp");
	List<ExBoxSystem> listadoExBox = (List<ExBoxSystem>) request.getAttribute("listadoExBox");
	List<Health> listadoHealth = (List<Health>) request.getAttribute("listadoHealth");
	List<FamilyAssignmentStretch> familyAssignmentStretchs = (List<FamilyAssignmentStretch>) request
			.getAttribute("FamilyAssignmentStretch");

	Agreement agreementEmp = (Agreement) request.getAttribute("agreementEmp");
	Employee employee = (Employee) request.getAttribute("Employee");
%>

<script lang="javascript">
	function onLoadPage(){
		/**<code>
< %for (RagreementAPV bsApvEmp : listadoApvEmp) {%>
			addApv('< %=bsApvEmp.getCurrency()%>','< %=bsApvEmp.getApv()%>','< %=BSWeb.formatDouble(request, bsApvEmp.getAmount())%>');
< %}%>
</code>*/
	}
	
	function beforeSubmit(){
		integerFocus(document.getElementById('MonthsQuoted'));
		doubleFocus(document.getElementById('AmountAccount2'));
		doubleFocus(document.getElementById('AdditionalPFMAmount'));
		doubleFocus(document.getElementById('HealthAmount'));
		integerFocus(document.getElementById('SimpleLoad'));
		integerFocus(document.getElementById('MaternalLoad'));
		integerFocus(document.getElementById('DisabilityBurdens'));
	
		var amountList = document.getElementsByName("apvAmount");
		var i;
		for(i in amountList){
			doubleFocus(amountList[i]);
		}
		
		return true;
	}
	
	function addApv(idCurrency, idApv, monto) {
		var rowCount = $("#apvs tr").length;
//		alert(monto);
		
		if (rowCount == 0) {
			cloneApv = $('#apvHide').clone();
//			alert(cloneApv.toString() + ' ' + cloneApv[0].tagName);

			cloneApv.attr('id', 'apv' + rowCount);
			
			$('#apvs').append(cloneApv);

			$('#apv' + rowCount).find("select[name='apvCurrency']").val(
					idCurrency);
			$('#apv' + rowCount).find("select[name='apvInstitution']").val(idApv);
			$('#apv' + rowCount).find("input[name='apvAmount']").val(monto);
		} else {
			var clon = $('#apvHide').clone();
			clon.attr('id', 'apv' + rowCount);
			clon.find('#eliminarHide').remove();
			newTD = "<td><a href='javascript:delApv("
					+ rowCount + ")'>Eliminar</a></td>";
			clon.append(newTD);
			apvId = rowCount == 1 ? "#" + $('#apvs tr:first').attr('id')
					: '#apv' + (rowCount - 1);
			$(apvId).after(clon);

			$('#apv' + rowCount).find("select[name='apvCurrency']").val(
					idCurrency);
			$('#apv' + rowCount).find("select[name='apvInstitution']").val(idApv);
			$('#apv' + rowCount).find("input[name='apvAmount']").val(monto);
		}
	}

	function delApv(idApv) {
		$('#apv' + idApv).remove();
	}

	/**<code>
	function getApvSelected() {
		apvs = $("#apvs");
		currencies = [];
		amounts = [];
		institutions = [];
		apvs.find('#apvCurrency').each(function() {
			currencies.push($(this).val());
		});
		apvs.find('#apvAmount').each(function() {
			amounts.push($(this).val());
		});
		apvs.find('#apvInstitution').each(function() {
			institutions.push($(this).val());
		});
	}
</code>*/
	function changePFM(o) {
		if (o.value > 1) {
			document.getElementById('exBox').value = 1;
		}
	}

	function changeExBox(o) {
		if (o.value > 1) {
			document.getElementById('afpEmp').value = 1;
		}
	}
</script>

<div class="page-header">
	<h1>Informaci�n previsional</h1>
</div>
<%
	String nextServlet = (String) request.getAttribute("Action");

	if ("insert".equalsIgnoreCase(nextServlet)) {
		nextServlet = "InsertRecord";
	} else {
		nextServlet = "UpdateRecord";
	}
%>
<c:import url="/servlet/dalea/web/GetEmployeeInfo" />

<br>
<div style="display: none" id="divHide">
	<table id="tableHide">
		<tr id="apvHide">

			<td><select name="apvCurrency" class="form-control">
					<option value="<%=Currency.CLF%>">UF</option>
					<option value="<%=Currency.CLP%>">Pesos</option>
					<option value="<%=Currency.PRC%>">%</option>
			</select></td>
			<td><input name="apvAmount" class="form-control"
				onfocus="javascript:doubleFocus(this);"
				onblur="javascript:doubleBlur(this);"
				value="<%=BSWeb.formatDouble(request, 0.0)%>"></td>

			<td class="cData"><select name="apvInstitution" class="form-control">
					< %
						for (APV bsApv : listadoApv) {
					%>
					<OPTION value="< %=bsApv.getId()%>">< %=bsApv.getName()%></OPTION>
					< %
						}
					%>
			</select></td>
			<td id="eliminarHide"><a href="javascript:delApv(0)">Eliminar</a></td>
		</tr>
	</table>
</div>

<form
	action="${pageContext.request.contextPath}/servlet/config/employee/SavePrevitionalInfo"
	method="post" id="editForm"
	onsubmit="javascript:return(beforeSubmit());">
	<input type="hidden" name="cId"
		value="<%=request.getParameter("cId")%>">


	<div class="well row">

		<div class="row">
			<div class="col-sm-2">
				<label for="afpEmp">AFP:</label>
			</div>
			<div class="col-sm-4">
				<select id="afpEmp" name="afpEmp" class="form-control"
					onchange="javascript:changePFM(this);">
					<%
						for (PFM bsAfp : listadoAfp) {
					%>
					<OPTION value="<%=bsAfp.getId()%>"
						<%=agreementEmp.getPfm() != null && bsAfp.getId().equals(agreementEmp.getPfm()) ? "selected" : ""%>><%=bsAfp.getName()%></OPTION>
					<%
						}
					%>
				</select>
			</div>
			<div class="col-sm-2">
				<label for="MonthsQuoted">Meses cotizados:</label>
			</div>
			<div class="col-sm-4">
				<input type="text" id="MonthsQuoted" name="MonthsQuoted"
					class="form-control" onfocus="javascript:integerFocus(this);"
					onblur="javascript:integerBlur(this);"
					value="<%=BSWeb.formatInteger(request, agreementEmp.getMonthsQuoted())%>">

			</div>
		</div>

		<div class="row ">
			<div class="col-sm-2">
				<label for="CurrencyAccount2">Moneda Cuenta 2:</label>
			</div>
			<div class="col-sm-4">
				<select name="CurrencyAccount2" id="CurrencyAccount2" class="form-control">
					<option value="<%=Currency.CLF%>" <%=agreementEmp.getAccount2Currency().equals(Currency.CLF)?"selected" : ""%>>UF</option>
					<option value="<%=Currency.CLP%>" <%=agreementEmp.getAccount2Currency().equals(Currency.CLP)?"selected" : ""%>>Pesos</option>
					<option value="<%=Currency.PRC%>" <%=agreementEmp.getAccount2Currency().equals(Currency.PRC)?"selected" : ""%>>%</option>
					
				</select>
			</div>
			<div class="col-sm-2 ">
				<label for="AmountAccount2">Monto Cuenta 2:</label>
			</div>
			<div class="col-sm-4 ">
				<input type="text" name="AmountAccount2" id="AmountAccount2"
					class="form-control" onfocus="javascript:doubleFocus(this);"
					onblur="javascript:doubleBlur(this);"
					value="<%=BSWeb.formatDouble(request, agreementEmp.getAccount2Amount().doubleValue())%>">
			</div>
		</div>

		<div class="row">
			<div class="col-sm-2 ">
				<label for="AdditionalPFMCurrency">Ahorro voluntario
					adicional:</label>
			</div>
			<div class="col-sm-4 ">
				<select name="AdditionalPFMCurrency" id="AdditionalPFMCurrency" class="form-control">
					<option value="<%=Currency.CLF%>" <%=agreementEmp.getAdditionalPFMCurrency().equals(Currency.CLF)?"selected" : ""%>>UF</option>
					<option value="<%=Currency.CLP%>" <%=agreementEmp.getAdditionalPFMCurrency().equals(Currency.CLP)?"selected" : ""%>>Pesos</option>
					<option value="<%=Currency.PRC%>" <%=agreementEmp.getAdditionalPFMCurrency().equals(Currency.PRC)?"selected" : ""%>>%</option>
				</select>
			</div>
			<div class="col-sm-2 ">
				<label for="AdditionalPFMAmount">Monto Ahorro adicional</label>
			</div>
			<div class="col-sm-4 ">
				<Input type="text" name="AdditionalPFMAmount"
					id="AdditionalPFMAmount" class="form-control"
					onfocus="javascript:doubleFocus(this);"
					onblur="javascript:doubleBlur(this);"
					value="<%=BSWeb.formatDouble(request, agreementEmp.getAdditionalPFMAmount().doubleValue())%>">
			</div>
		</div>
		<div class="row">&nbsp;</div>

		<div class="row">
			<div class="col-sm-2 ">
				<label for="health">Sistema de salud:</label>
			</div>
			<div class="col-sm-4">
				<select id="health" name="health" class="form-control">
					<%
						for (Health bsHealth : listadoHealth) {
					%>
					<OPTION value="<%=bsHealth.getId()%>"
						<%=agreementEmp.getHealth() != null && bsHealth.getId().equals(agreementEmp.getHealth()) ? "selected" : ""%>><%=bsHealth.getName()%></OPTION>
					<%
						}
					%>
				</select>
			</div>


			<div class="col-sm-2">
				<label for="exBox">Caja Ex-R�gimen:</label>
			</div>
			<div class="col-sm-4">
				<select id="exBox" name="exBox" class="form-control"
					onchange="javascript:changeExBox(this);">
					<%
						for (ExBoxSystem bsExbox : listadoExBox) {
					%>
					<OPTION value="<%=bsExbox.getId()%>"
						<%=agreementEmp.getExBoxSystem() != null && bsExbox.getId().equals(agreementEmp.getExBoxSystem())
						? "selected" : ""%>><%=bsExbox.getName()%></OPTION>
					<%
						}
					%>
				</select>
			</div>
		</div>

	</div>
	<div class="row well">
		<div class="row">
			<div class="col-sm-12">
				<span class="alert alert-info">Complementario de salud (El
					complementario de salud solo aplica con valor mayor a 0)</span>
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">
			<div class="col-sm-2 ">
				<label for="HealthCurrency">Moneda:</label>
			</div>
			<div class="col-sm-4 ">
				<select name="HealthCurrency" id="HealthCurrency" class="form-control">
					<option value="<%=Currency.CLF%>" <%=agreementEmp.getHealthCurrency().equals(Currency.CLF)?"selected" : ""%>>UF</option>
					<option value="<%=Currency.CLP%>" <%=agreementEmp.getHealthCurrency().equals(Currency.CLP)?"selected" : ""%>>Pesos</option>
					<option value="<%=Currency.PRC%>" <%=agreementEmp.getHealthCurrency().equals(Currency.PRC)?"selected" : ""%>>%</option>
				</select>
			</div>
			<div class="col-sm-2 ">
				<label for="HealthAmount">Monto:</label>
			</div>
			<div class="col-sm-4 ">
				<input name="HealthAmount" id="HealthAmount" class="form-control"
					onfocus="javascript:doubleFocus(this);"
					onblur="javascript:doubleBlur(this);"
					value="<%=BSWeb.formatDouble(request, agreementEmp.getHealthAmount().doubleValue())%>">
			</div>
		</div>

		<div class="row">&nbsp;</div>
		<div class="row">
			<div class="col-sm-2 ">
				<label for="FamilyAssignmentStretch">Tramo asignaci�n
					familiar:</label>
			</div>
			<div class="col-sm-10">
				<select name="FamilyAssignmentStretch" id="FamilyAssignmentStretch" class="form-control">
					<%
						for (FamilyAssignmentStretch familyAssignmentStretch : familyAssignmentStretchs) {
					%>
					<OPTION value="<%=familyAssignmentStretch.getId()%>"
						<%=agreementEmp.getFamilyAssignmentStretch() != null
						&& familyAssignmentStretch.getId().equals(agreementEmp.getFamilyAssignmentStretch()) ? "selected" : ""%>><%=familyAssignmentStretch.getKey()%></OPTION>
					<%
						}
					%>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2 ">
				<label for="SimpleLoad">Cargas simples:</label>
			</div>
			<div class="col-sm-4 ">
				<input id="SimpleLoad" name="SimpleLoad" class="form-control"
					onfocus="javascript:integerFocus(this);"
					onblur="javascript:integerBlur(this);"
					value="<%=BSWeb.formatInteger(request, agreementEmp.getSimpleLoads())%>">
			</div>

			<div class="col-sm-2 ">
				<label for="MaternalLoad">Cargas maternales:</label>
			</div>
			<div class="col-sm-4 ">
				<input id="MaternalLoad" name="MaternalLoad" class="form-control"
					onfocus="javascript:integerFocus(this);"
					onblur="javascript:integerBlur(this);"
					value="<%=BSWeb.formatInteger(request, agreementEmp.getMaternalLoads())%>">
			</div>
		</div>

		<div class="row">
			<div class="col-sm-2 ">
				<Label for="DisabilityBurdens">Cargas invalidez:</Label>
			</div>

			<div class="col-sm-4 ">
				<input id="DisabilityBurdens" name="DisabilityBurdens"
					class="form-control" onfocus="javascript:integerFocus(this);"
					onblur="javascript:integerBlur(this);"
					value="<%=BSWeb.formatInteger(request, agreementEmp.getDisabilityBurdens())%>">
			</div>

			<div class="col-sm-2 ">
				<label for="Pensionary">Pensionado:</label>
			</div>
			<div class="col-sm-4 ">
				<%
					Boolean pensionary = agreementEmp.getPensionary();
				%>
				<select Name="Pensionary" id="Pensionary" class="form-control">
					<option value="false" <%=(!pensionary ? "selected" : "")%>>No</option>
					<option value="true" <%=(pensionary ? "selected" : "")%>>Si</option>
				</select>
			</div>
		</div>
	</div>

<!-- 
	<div class="row">
		<table
			class="table table-striped table-bordered table-hover table-condensed table-responsive">
			<caption>
				APV (< %=listadoApvEmp.size()%>)
			</caption>
			<thead>
				<tr>
					<th>Moneda APV</th>
					<th>Monto</th>
					<th>Instituci�n</th>
					<th>Acci�n</th>
				</tr>
			</thead>
			<tbody id="apvs">
			</tbody>
		</table>
	</div>
 -->
</form>

<div class="row">
	<button type="button" class="btn btn-primary"
		onclick="javascript:$('#editForm').submit();">Aceptar</button>
	&nbsp;&nbsp;&nbsp;
	<!-- 
	<button type="button" class="btn btn-default"
		onclick="javascript:addApv(< %=Currency.CLF%>, < %=listadoApv.get(0).getId()%>, '< %=BSWeb.formatDouble(request, 0.0)%>')">Agregar
		APV</button>	
	&nbsp;&nbsp;&nbsp;
-->
	<button class='btn btn-link'
		onclick="returnTo('${pageContext.request.contextPath}/servlet/config/employee/EmployeeAgreementManager');">Volver</button>
</div>
<div class="row">&nbsp;</div>
<!-- 
<a class="cCancel" href="${pageContext.request.contextPath}/servlet/config/employee/InformationPrevitional">Cancelar</a>&nbsp;&nbsp;&nbsp;
 -->
 
