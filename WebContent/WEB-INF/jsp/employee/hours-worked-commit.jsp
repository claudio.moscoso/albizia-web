<%@page import="cl.buildersoft.framework.util.BSWeb"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript"
	src="${applicationScope['ALBIZIA_CONTEXT']}/js/employee/hours-worked-commit.js?<%=BSWeb.randomString()%>"></script>

<div class="page-header">
	<h1>Horas trabajadas</h1>
</div>

<div class="row">
	<label class="col-sm-2">Periodo:</label>
	<c:out value="${requestScope.Period}" />
</div>


<div class="row">
	<table id="DataTable"
		class="table table-striped table-bordered table-hover table-condensed table-responsive hover compact">
		<thead>
			<tr>
				<th>Rut</th>
				<th>Nombre</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.Employee}" var="row">
				<tr style="cursor: pointer;">
					<td>${row.rut}</td>
					<td>${row.name}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<form class="form-horizontal" method="post" id="form">
	<input type="hidden" name="PeriodId" value="${requestScope.PeriodId}">

	<c:forEach items="${requestScope.Employee}" var="row">
		<input type="hidden" name="EmployeeId" value="${row.employeeId}">
	</c:forEach>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="Hours">Horas
			Mensuales:</label>
		<div class="col-sm-5">
			<input class="form-control" id="Hours" type="text" name="Hours"
				value="180">
		</div>
	</div>
</form>
<button class="btn btn-default" id="changeHoursButton" 
	onclick="javascript:doAction()">Aceptar</button>