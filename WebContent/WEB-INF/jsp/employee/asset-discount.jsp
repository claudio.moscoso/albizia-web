<%@page import="java.util.List"%>
<%@page import="cl.buildersoft.albizia.business.beans.AgreementDiscount"%>
<%@page import="cl.buildersoft.albizia.business.beans.AgreementAsset"%>
<%@page import="cl.buildersoft.dalea.bean.Agreement"%>
<%@page import="cl.buildersoft.dalea.bean.Employee"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	Employee employee = (Employee) request.getAttribute("Employee");
	Agreement agreement = (Agreement) request.getAttribute("Agreement");
	List<AgreementAsset> assetList = (List<AgreementAsset>) request.getAttribute("Asset");
	List<AgreementDiscount> discountList = (List<AgreementDiscount>) request.getAttribute("Discount");

	/**
	String focus = request.getParameter("cType");
	String assetActive = "";
	String discountActive = "";

	if ("".equals(focus) || focus == null) {
		focus = "Asset";
	}
	if (focus.equals("Asset")) {
		assetActive = "active";
		discountActive = "";
	} else {
		assetActive = "";
		discountActive = "active";
	}
	*/
%>
<fmt:setLocale value="es_ES" />
<script
	src="${pageContext.request.contextPath}/js/assetDiscount/asset-discount.js?<%=Math.random()%>">
	
</script>
<!-- 
<script>
	var focus = '<c:out value="${cType}"></c:out>';
	if (focus == '') {
		focus = 'Asset';
	}
</script>
 -->

<div class="page-header">
	<h1>Haberes y descuentos para empleado.</h1>
</div>

<c:import url="/servlet/dalea/web/GetEmployeeInfo" />
<!-- 
<div class="row">
	<div class="col-sm-12 well">
		<label>Per�odo:</label>&nbsp;${requestScope.PeriodName}
	</div>
</div>

 
<form method="post" action="${pageContext.request.contextPath}/servlet/ShowParameters">
 -->
<hr>

<form method="post" id="frm"
	xaction="${applicationScope['DALEA_CONTEXT']}/servlet/ShowParameters"
	action="${pageContext.request.contextPath}/servlet/manager/employee/AssetDiscountSave">
	<!-- 
	<input name="cPeriod" type="hidden" value="${requestScope.Period.id}">
 -->
	<input name="cEmployee" type="hidden"
		value="${requestScope.Employee.id}"> <input
		name="AssetDiscount" id="AssetDiscount" type="hidden" value="ASSET">
	<input name="Agreement" type="hidden"
		value="${requestScope.Agreement.id}">

	
<ul class="nav nav-tabs">
		<li class='active'><a data-toggle="tab"
			href="#asset-content" onclick="javascript:clickTab('ASSET');">Abonos</a></li>
		<li><a
			data-toggle="tab" href="#discount-content"
			onclick="javascript:clickTab('DISCOUNT');">Descuentos</a></li>
	</ul>
	<!-- 
	+
	<c:out value="${requestScope.Agreement.id}" />
	+
 -->

	<div class="tab-content">
		<div id="asset-content" class="tab-pane fade in active">
			<table id="asset-table"
				class="table table-striped table-bordered table-hover table-condensed table-responsive">
				<thead>
					<tr>
						<td>Tipo</td>
						<td>Llave</td>
						<td>Nombre</td>
						<td>Moneda</td>
						<td>Monto</td>
						<td>Acci�n</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${requestScope.Asset}" var="row">
						<tr>
							<td><c:out value="${row.assetType.toText()}" /></td>
							<td><c:out value="${row.key}" /></td>
							<td><c:out value="${row.label}" /></td>
							<td><c:out value="${row.currency.toText()}" /></td>
							<td><fmt:formatNumber type="number" pattern="#,###,###.####"
									value="${row.amount}" /></td>
							<td><button type="button" class="btn btn-default"
									onclick="deleteRow('${row.id}','Asset')">Eliminar</button></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div id="discount-content" class="tab-pane fade">
			<table id="discount-table"
				class="table table-striped table-bordered table-hover table-condensed table-responsive">
				<thead>
					<tr>
						<td>Tipo</td>
						<td>Llave</td>
						<td>Nombre</td>
						<td>Moneda</td>
						<td>Monto</td>
						<td>Acci�n</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${requestScope.Discount}" var="row">
						<tr>
							<td><c:out value="${row.discountType.toText()}" /></td>
							<td><c:out value="${row.key}" /></td>
							<td><c:out value="${row.label}" /></td>
							<td><c:out value="${row.currency.toText()}" /></td>
							<td><fmt:formatNumber type="number" pattern="#,###,###.####"
									value="${row.amount}" /></td>
							<td><button type="button" class="btn btn-default"
									onclick="deleteRow('${row.id}','Discount')">Eliminar</button></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<button type="button" class="btn btn-default" id="newButton"
			onclick="javascript:newRow()">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			Agregar
		</button>
		<button type="button" class="btn btn-default" style="display: none"
			id="acceptButton"
			onclick="javascript:doubleFocus(document.getElementById('Amount'));document.getElementById('frm').submit();">
			<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			Confirmar
		</button>
		<button type="button" class="btn btn-default" style="display: none"
			id="cancelButton" onclick="javascript:cancelNew()">
			<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
			Descartar
		</button>
	</div>
</form>

<div class="row">&nbsp;</div>
<div class="row">
	<button type="button" onclick="javascript:sendForm()"
		class="btn btn-default">Aceptar</button>
	&nbsp;&nbsp;
	<button class='btn btn-link'
		onclick="returnTo('${pageContext.request.contextPath}/servlet/config/employee/EmployeeAgreementManager');">Volver</button>
</div>
<div style="display: none;" id="AssetTypeContainer">
	<select name="AssetType" class="form-control"><option
			value="ASSIGNMENT">Asignaci�n</option>
		<option value="NORMAL">Normal</option></select>
</div>
<div style="display: none;" id="DiscountTypeContainer">
	<select name="DiscountType" class="form-control">

		<option value='LEGAL'>Legal</option>
		<option value='PREVISIONAL'>Previsional</option>
		<option value='JUDICIAL'>Judicial</option>
		<option value='AFTER_REACHED_LIQUID'>Despues de alcance
			l�quido</option>
		<option value='COMPANY_PAID'>Empresa Paga</option>
	</select>
</div>

<div style="display: none;" id="AssetCurrencyContainer">
	<select name="Currency" class="form-control"><option
			value="CLP">Pesos CL</option>
		<option value="PERCENT">Porcentaje</option>
		<option value="CLP">Unidad de Fomento (UF)</option></select>
</div>

<form method="post"
	action="${pageContext.request.contextPath}/servlet/manager/employee/AssetDiscountDelete"
	id="DeleteForm">
	<input name="cId" type="hidden" value="${requestScope.Employee.id}">
	<input name="cAssetDicountId" id="cId" type="hidden"> <input
		name="cType" id="cType" type="hidden">
</form>