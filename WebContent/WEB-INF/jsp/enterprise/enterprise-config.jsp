<%@page import="cl.buildersoft.dalea.bean.EnterpriseConfig"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	EnterpriseConfig enterpriseConfig = (EnterpriseConfig) request.getAttribute("EnterpriseConfig");
%>

<div class="page-header">
	<h1>Configuraci�n de la empresa</h1>
</div>

<form
	action="${applicationScope['ALBIZIA_CONTEXT']}/servlet/albizia/enterprise/SaveEnterpriseConfig"
	class="form-inline" method="post" id="editForm">
	<input type="hidden" name="cId"
		value="<%=request.getParameter("cId")%>"> <input type="hidden"
		name="enterprise" value="<%=enterpriseConfig.getEnterprise()%>">

	<div class="row">
		<div class='col-sm-6 col-sm-offset-1'>
			<label for="GratificationType">Tipo de Gratification</label> <select
				class="form-control" id="GratificationType" name="GratificationType">
				<c:forEach items="${requestScope.GratificationTypeList}"
					var="GratificationType">


					<c:choose>
						<c:when
							test="GratificationType.id == requestScope.EnterpriseConfig.gratificationType">
							<option value="${GratificationType.id}" selected="">${GratificationType.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${GratificationType.id}">${GratificationType.name}</option>
						</c:otherwise>
					</c:choose>


					<!-- 					<option value="${GratificationType.id}"
						<option value="${GratificationType.id}"
						<c:if test="${GratificationType.id == requestScope.EnterpriseConfig.gratificationType}">selected</c:if>>${GratificationType.name}</option>
						 -->
				</c:forEach>

			</select>
		</div>
		<div class='col-sm-4'>
			<label for="GratificationAmount">Monto Gratificacion</label> <input
				class="form-control" type="text" diabled id="GratificationAmount">
		</div>

	</div>

	<div class="row">
		<%=paintConcept("showDateUfUtm", enterpriseConfig.getShowDateUfUtm(), "Mostrar Fecha, UF y UTM",
					false)%>
		<%=paintConcept("showProfile", enterpriseConfig.getShowProfile(), "Mostrar Cargo", true)%>
	</div>
	<div class="row">
		<%=paintConcept("showCostCenter", enterpriseConfig.getShowCostCenter(), "Mostrar Centro de Costos",
					false)%>
		<%=paintConcept("showDataAgreement", enterpriseConfig.getShowDataAgreement(),
					"Mostrar Datos del Contrato", true)%>
	</div>
	<div class="row">
		<%=paintConcept("showSalary", enterpriseConfig.getShowSalaryRoot(), "Mostrar Renta Base", false)%>
		<%=paintConcept("showEmployerBonus", enterpriseConfig.getShowEmployerBonus(),
					"Mostrar Aportes del Empleador", true)%>
	</div>
	<div class="row">
		<%=paintConcept("showWorkDay", enterpriseConfig.getShowWorkDay(), "Mostrar D�as Trabajados", false)%>
		<%=paintConcept("showNetPaymentScope", enterpriseConfig.getShowNetPaymentScope(),
					"Mostrar Alcance L�quido", true)%>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="col-sm-11 col-sm-offset-1">
			<label for="textFootSalary">Texto al Pie de la Liquidaci�n:</label>
		</div>

		<div class="col-sm-10 col-sm-offset-1">
			<textarea name="textFootSalary" id="textFootSalary" cols="80"
				rows="4" style="width: 100%"><%=enterpriseConfig.getTextFootSalary()%></textarea>
		</div>
		<div class="col-sm-1"></div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="col-sm-11 col-sm-offset-1">
			Avisar todos los dias lunes al correo <input id="email" name="email"
				value="<%=enterpriseConfig.getMailNotice()%>"> los contratos
			que vencen dentro de los proximos 15 d�as.
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="col-sm-11 col-sm-offset-1">
			Los empleados pueden consultar las �ltimas
			<%=paintCombo(enterpriseConfig.getViewLastSettlements())%>
			liquidaciones por la web.
		</div>
	</div>
</form>
<button type="button" onclick="javascript:$('#editForm').submit();"
	class="btn btn-primary">Aceptar</button>
<button class='btn btn-link'
	onclick="returnTo('${applicationScope['ALBIZIA_CONTEXT']}/servlet/albizia/manager/EnterpriseManager');">Cancelar</button>

&nbsp;
<br>


<%!private String paintConcept(String id, boolean check, String label, boolean isRight) {
		String out = "<div class='col-sm-" + (isRight ? "5" : "3") + " col-sm-offset-" + (isRight ? "3" : "1") + "'>"
				+ paintCheck(id, check) + "&nbsp;<label for='" + id + "'>" + label + "</div>";
		return out;
	}

	private String paintCheck(String id, boolean check) {
		String isChecked = check ? "checked" : "";
		return "<input type='checkbox' id='" + id + "' name='" + id + "' " + isChecked + ">";
	}

	private String paintCombo(Integer viewLastSettlements) {
		String out = "<select name='cViewLastSettlements'>";
		Integer[] items = { 1, 3, 6, 12, 24 };

		for (Integer item : items) {
			out += "<option value='" + item + "' " + (viewLastSettlements.equals(item) ? "selected" : "") + ">" + item
					+ "</option>";
		}
		out += "</select>";

		return out;
	}%>
