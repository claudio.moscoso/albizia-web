<%@page import="cl.buildersoft.dalea.bean.Period"%>
<%@page import="cl.buildersoft.framework.database.BSmySQL"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="cl.buildersoft.framework.util.BSDateTimeUtil"%>
<%@page import="java.util.Date,cl.buildersoft.framework.util.BSWeb"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	Period period = (Period) request.getAttribute("Period");
	Date date = period.getDate();

	//	ResultSet book = (ResultSet) request.getAttribute("Book");

	//	Connection conn = (Connection) request.getAttribute("Conn");
%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/remuneration/salary/process-salary.js?<%=BSWeb.randomString()%>">
	
</script>

<div class="page-header">
	<h1>C�lculo de Remuneraciones.</h1>
</div>

<form id="CalculateSalary"
	action="${pageContext.request.contextPath}/servlet/remuneration/process/salary/CalculateSalary"
	method="post">

	<div class="form-group">
		<label class="col-sm-1 control-label">Per�odo:</label>
		<div class="col-sm-4">
			<input
				value="<%=BSDateTimeUtil.month2Word(date)%> de <%=BSDateTimeUtil.getYear(date)%>"
				disabled class="form-control" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-1 control-label">Empresa:</label>
		<div class="col-sm-4">
			<select name="Enterprise" class="form-control"><c:forEach
					items="${requestScope.EnterpriseList}" var="row">
					<option value="${row.id}">${row.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>

	<button class="col-sm-2 btn btn-default">Aceptar</button>
	<br>
	<hr>
	<div class="row">
		<table id="TableSettlement"
			class="table table-striped table-bordered table-hover table-condensed table-responsive hover compact">
			<thead>
				<tr>
					<th>Id</th>
					<th>Rut</th>
					<th>Nombre</th>
					<th>S. Base</th>
					<th>Horas Extras</th>
					<th>Liquido</th>
				</tr>
			</thead>

			<tbody>
				<c:forEach items="${requestScope.EmployeeList}" var="employee">
					<tr>
						<td>${employee.id}</td>
						<td>${employee.rut}</td>
						<td>${employee.name}</td>
						<td>${employee.baseSalary}</td>
						<td>${employee.overtimeTime}</td>
						<td>${employee.liquidSalary}</td>

					</tr>
				</c:forEach>
 
			</tbody>
		</table>
	</div>


	<!-- 
	<table>

		<tr>
			<td class='control-label'>Per�odo:</td>
			<td class='cData'><%=BSDateTimeUtil.month2Word(date)%> de <%=BSDateTimeUtil.getYear(date)%></td>
		</tr>

		<tr>
			<td class='cLabel'>Empresa:</td>
			<td class='cData'><select name="Enterprise">
					<c:forEach items="${requestScope.EnterpriseList}" var="row">
						<option value="${row.id}">${row.name}</option>
					</c:forEach>
			</select></td>
		</tr>

	</table>
	

	<br> <br> <br>
		 -->
	<button class="btn btn-primary" type="button"
		onclick="javascript:doCalculate()">Calcular</button>
	<button class="btn" type="button"
		onclick="javascript:$('#downloadAsFile').submit();">Descargar
		como previred</button>
	<button class="btn">Enviar por Correo</button>
	<button class="btn">Descargar Liquidaci�n</button>
</form>

<form id="downloadAsFile"
	action="${pageContext.request.contextPath}/servlet/remuneration/process/salary/DownloadAsFile"
	method="post"></form>

<!-- 
<div class="row">
	<div id="col-sm-12" style="overflow: auto; position: relative;">
		Tabla con Books</div>
</div>
 -->


