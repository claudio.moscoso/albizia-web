<%@page import="cl.buildersoft.framework.util.BSWeb"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="cl.buildersoft.framework.util.BSConnectionFactory"%>
<%@page import="cl.buildersoft.dalea.bean.GratificationType"%>
<%@page import="cl.buildersoft.dalea.bean.ContractType"%>
 
<%@page import="cl.buildersoft.dalea.services.impl.AgreementServiceImpl"%>
<%@page import="cl.buildersoft.dalea.services.AgreementService"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="cl.buildersoft.dalea.bean.Employee"%>
<%@page import="cl.buildersoft.dalea.bean.Position"%>
<%@page import="cl.buildersoft.dalea.bean.Agreement"%>
<%@page import="cl.buildersoft.framework.util.BSDateTimeUtil"%>
<%@page import="cl.buildersoft.framework.database.BSmySQL"%>
<%
	Employee employee = (Employee) request.getAttribute("Employee");
	Agreement agreement = (Agreement) request.getAttribute("Agreement");
	if (agreement == null) {
		BSConnectionFactory cf = new BSConnectionFactory();

		Connection conn = cf.getConnection(request);

		AgreementService agreementService = new AgreementServiceImpl();
		agreement = agreementService.getDefaultAgreement(conn, employee.getId());
		cf.closeConnection(conn);
	}

	List<Position> positions = (List<Position>) request.getAttribute("Position");
	List<ContractType> contractTypes = (List<ContractType>) request.getAttribute("ContractTypes");
	List<GratificationType> gratificationTypes = (List<GratificationType>) request
			.getAttribute("GratificationType");
//	List<Horary> horaries = (List<Horary>) request.getAttribute("Horary");

	String dateFormat = (String) request.getAttribute("DateFormat");
%>

<div class="page-header">
	<h1>Informaci�n Contractual</h1>
</div>

<c:import url="/servlet/dalea/web/GetEmployeeInfo" />

<script type="text/javascript"
	src="${applicationScope['ALBIZIA_CONTEXT']}/js/config/employee/contractual-info.js?<%=BSWeb.randomString()%>">
</script>


<form id="frmContractualInfo" method="post" class="form-horizontal"
	action="${applicationScope['ALBIZIA_CONTEXT']}/servlet/config/employee/SaveContractualInfo">
	<input type="hidden" name="cId" value="<%=employee.getId()%>">

	<div class="form-group">
		<label for="cSalaryRoot" class="control-label col-sm-2">Sueldo
			Base:</label>
		<div class="col-sm-4">
			<input type="text" name="cSalaryRoot" id="cSalaryRoot"
				class="form-control " onfocus="javascript:integerFocus(this);"
				onblur="javascript:integerBlur(this);"
				value="<%=BSWeb.formatInteger(request, agreement.getSalaryBase().intValue())%>">
		</div>
		
		<label for="cHiredAmount" class="control-label col-sm-2">Tiempo contrato:</label>
		<div class="col-sm-2">
			<input type="text" name="cHiredAmount" id="cHiredAmount"
				class="form-control" onfocus="javascript:integerFocus(this);"
				onblur="javascript:integerBlur(this);"
				value="<%=BSWeb.formatInteger(request, agreement.getHiredAmount())%>">
				</div>
				<div class="col-sm-2">
				<c:set var="HiredType" value="${Agreement.hiredType}"/>
				
				<select name="cHiredType" id="cHiredType" class="form-control">
					<c:choose>
						<c:when test="${HiredType == 'H'}">
							<option value="D">D�as</option>
							<option value="H" selected>Horas</option>
						</c:when>
						<c:otherwise>
							<option value="D" selected>D�as</option>
							<option value="H">Horas</option>
						</c:otherwise>
					</c:choose>
				
				</select>
		</div>
	</div>

	<div class="form-group">
		<label for="cPosition" class="control-label col-sm-2">Cargo:</label>
		<div class="col-sm-4">
			<select name="cPosition" id="cPosition" class="form-control ">
				<%
					for (Position position : positions) {
				%>
				<option value="<%=position.getId()%>"
					<%=position.getId().equals(agreement.getPosition()) ? "selected" : ""%>><%=position.getName()%></option>
				<%
					}
				%>
			</select>
		</div>

		<label for="cContractType" class="control-label col-sm-2">Tipo
			de Contrato:</label>
		<div class="col-sm-4">
			<select name="cContractType" id="cContractType" class="form-control ">
				<%
					for (ContractType contractType : contractTypes) {
				%>
				<option value="<%=contractType.getId()%>"
					<%=contractType.getId().equals(agreement.getContractType()) ? "selected" : ""%>><%=contractType.getName()%></option>
				<%
					}
				%>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="cStartContract" class="control-label col-sm-2">Inicio
			Contrato:</label>
		<div class="col-sm-4">
			<input type="text" name="cStartContract" id="cStartContract"
				class="form-control "
				value="<%=BSDateTimeUtil.date2String(request, agreement.getStartContract())%>">
		</div>

		<label for="cEndContract" class="control-label col-sm-2">T�rmino
			Contrato:</label>
		<div class="col-sm-4">
			<input type="text" name="cEndContract" id="cEndContract"
				class="form-control "
				value="<%=BSDateTimeUtil.date2String(request, agreement.getEndContract())%>">
		</div>
	</div>
	<div class="form-group">

		<label for="cFeeding" class="control-label col-sm-2">Colaci�n:</label>
		<div class="col-sm-4">
			<input type="text" name="cFeeding" id="cFeeding"
				class="form-control " onfocus="javascript:doubleFocus(this);"
				onblur="javascript:doubleBlur(this);"
				value="<%=BSWeb.formatDouble(request, agreement.getFeeding().doubleValue())%>">
		</div>
		<label for="cMobilization" class="control-label col-sm-2">Movilizaci�n:</label>
		<div class="col-sm-4">
			<input type="text" name="cMobilization" id="cMobilization"
				class="form-control " onfocus="javascript:doubleFocus(this);"
				onblur="javascript:doubleBlur(this);"
				value="<%=BSWeb.formatDouble(request, agreement.getMobilization().doubleValue())%>">
		</div>


	</div>

</form>
<!-- 
		<div class="row">
			<div class="col-sm-2 ">Gratificaci�n:</div>
			<div class="col-sm-4 ">
				<select name="cGratificationType">
					< %for (GratificationType gratificationType : gratificationTypes) {%>
					<option value="< %=gratificationType.getId()%>"
						< %=gratificationType.getId().equals(agreement.getGratificationType()) ? "selected" : ""%>>< %=gratificationType.getName()%></option>
					< %}%>
				</select>
			</div>
			<div class="col-sm-2 ">Horario:</div>
			<div class="col-sm-4 ">
				<select name="cHorary">
					< %for (Horary horary : horaries) {%>
					<option value="< %=horary.getId()%>"
						< %=horary.getId().equals(agreement.getHorary()) ? "selected" : ""%>>< %=horary.getName()%></option>
					< %}%>
				</select>
			</div>
		</div>
-->
<!--  
		<div class="row">
			<div class="col-sm-2 ">Movilizaci�n:</div>
			<div class="col-sm-4 ">
				<input type="text" name="cMobilization" id="cMobilization"
					onfocus="javascript:doubleFocus(this);"
					onblur="javascript:doubleBlur(this);"
					value="<%=BSWeb.formatDouble(request, agreement.getMobilization().doubleValue())%>">
			</div>
		</div>
		
<div class="row">
	<div class="col-sm-2 ">Sueldo Base:</div>
	<div class="col-sm-4 ">
		<input type="text" name="cSalaryRoot" id="cSalaryRoot"
			onfocus="javascript:integerFocus(this);"
			onblur="javascript:integerBlur(this);"
			value="<%=BSWeb.formatInteger(request, agreement.getSalaryBase().intValue())%>">

	</div>

</div>


</div>
-->
<div class="row">
	<button type="button" class="btn btn-primary"
		onclick="javascript:validateAndSubmit()">Aceptar</button>
	&nbsp;&nbsp;
	<button class='btn btn-link' type="button"
		onclick="returnTo('${applicationScope['ALBIZIA_CONTEXT']}/servlet/config/employee/EmployeeAgreementManager');">Volver</button>

</div>
<!-- 
</form>
 -->

<span id="ErrorMessage" class="cError"></span>
