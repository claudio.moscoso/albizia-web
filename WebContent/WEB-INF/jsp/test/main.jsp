<%
	javax.servlet.ServletContext context = application.getContext("/dalea-web");

	javax.servlet.RequestDispatcher rdHeader = context.getRequestDispatcher("/WEB-INF/jsp/common/header3.jsp");
	javax.servlet.RequestDispatcher rdFooter = context.getRequestDispatcher("/WEB-INF/jsp/common/footer3.jsp");
	
	rdHeader.include(request, response);
	getServletContext().getRequestDispatcher("/WEB-INF/jsp/common/menu2.jsp").include(request, response);
	getServletContext().getRequestDispatcher("/WEB-INF/jsp/test/data-table2.jsp").include(request, response);
	
	rdFooter.include(request, response);
%>
