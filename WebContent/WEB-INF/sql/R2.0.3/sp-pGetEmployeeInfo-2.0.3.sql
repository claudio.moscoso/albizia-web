DROP PROCEDURE if exists pGetEmployeeInfo;
DELIMITER $$

CREATE PROCEDURE pGetEmployeeInfo(IN vId BIGINT(20))
BEGIN
SELECT	
	e.cId, 
	e.cRut, 
	e.cName, 
	tPosition.cId as cPost, 
	a.cId as cArea, 
	e.cPrivilege, 
	e.cUsername, 
	a.cName AS cAreaName, 
	a.cKey AS cCC,
	IFNULL((SELECT	t.cName
			FROM	tR_EmployeeTurn AS r
			LEFT JOIN tTurn AS t ON r.cTurn = t.cId
			WHERE	r.cEmployee = e.cId
			LIMIT 1
	), 'Diferido') AS cTurnName,
	IFNULL((	SELECT	CONCAT(t.cStartTime, ' - ', t.cEndTime) 
		FROM	tR_EmployeeTurn AS r
		LEFT JOIN tTurnDay AS t ON r.cTurn = t.cTurn
		WHERE	r.cEmployee = e.cId AND cDay = 1 
		LIMIT 1
	),'') AS cHorary
	FROM 	tEmployee AS e
	LEFT JOIN tAgreement as tAgreement on e.cId = tAgreement.cEmployee
	LEFT JOIN tPosition as tPosition on tAgreement.cPosition = tPosition.cId
	LEFT JOIN tcostcenter as tCostCenter on e.cCostCenter = tcostcenter.cid
	LEFT JOIN tArea AS a ON tCostCenter.cArea = a.cId
	WHERE	e.cId = vId;
END$$

DELIMITER ;