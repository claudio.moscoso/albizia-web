@echo off
echo -- 2.0.3 --
IF "%1" == "" GOTO error
	echo "Creando funciones de sistema..."
	for %%i in (fn-*.sql) do mysql -D%1 -t -u root --default-character-set=utf8 < %%i

	echo "Creando procedimientos de sistema..."
	for %%i in (sp-*.sql) do mysql -D%1 -t -u root --default-character-set=utf8 < %%i

	echo "Actualizando estructuras y datos de base de datos"
	mysql -D%1 -t -u root --default-character-set=utf8 < update-database.sql

goto fin

:error

echo "No se indico nombre de la base de datos, ejecute: "
echo "$ run-all timectrl"

:fin

