DROP PROCEDURE if exists pPurgeeEmployee;
DELIMITER $$

CREATE PROCEDURE pPurgeeEmployee(IN pId BIGINT)
BEGIN	
	delete from tAgreement where cEmployee = pId;
#	delete from tBook where cEmployee = pId;
	delete from tCrewProcess where cEmployee = pId;
	delete from tFile where cEmployee = pId;
	delete from tFingerprint where cEmployee = pId;
	delete from tHoliday where cEmployee = pId;
	delete from tLicense where cEmployee = pId;
	delete from tCrewProcess where cEmployee = pId;
	delete from tOvertime where cEmployee = pId;
	delete from tProgressive where cEmployee = pId;
	delete from tR_EmployeeTurn where cEmployee = pId;
	
	update tEmployee SET cBoss=null WHERE cBoss=pId;
	
	delete from tEmployee where cId = pId;
END$$

DELIMITER ;
