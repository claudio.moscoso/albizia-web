#!/bin/bash

echo -- 2.0.2 --

if [ -z $1 ]
then
   echo Sin parametros
   
else
	for value in fn-*.sql
	do
	   echo processing $value file.
	   mysql -D$1 -t -u root --default-character-set=utf8 < $value
	done
	for value in sp-*.sql
	do
	   echo processing $value file.
	   mysql -D$1 -t -u root --default-character-set=utf8 < $value
	done
	
 	echo processing update-database.sql file.
	mysql -D$1 -t -u root --default-character-set=utf8 < update-database.sql
   
fi 


