#!/bin/bash

cwd=$(pwd)
cd $GDRIVE_PATH/timecontrol-install/RespaldoDB/desarrollo
echo Executing $PWD/restore-backup.sh
./restore-backup.sh $1 1.3.3

echo Jumping to $cwd
cd $cwd 

echo Executing run-once.sh
./run-once.sh $1

echo DONE!