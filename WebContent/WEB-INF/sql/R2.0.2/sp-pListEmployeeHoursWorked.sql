DROP PROCEDURE if exists pListEmployeeWorkedHours;
DROP PROCEDURE if exists pListEmployeeHoursWorked;
DELIMITER $$

CREATE PROCEDURE pListEmployeeHoursWorked(IN pPeriod BIGINT)
BEGIN
	SELECT b.cId, a.cId as cEmployeeId, a.cRut, a.cName, IFNULL(b.cAmount, 0) AS cAmount
	FROM tEmployee as a
	LEFT JOIN tHoursWorked as b on a.cId = b.cEmployee AND b.cPeriod = pPeriod
	WHERE a.cEnabled = true;

END$$

DELIMITER ;

