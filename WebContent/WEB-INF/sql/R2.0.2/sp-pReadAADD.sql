DROP PROCEDURE if exists pReadAADD;
DELIMITER $$

CREATE PROCEDURE pReadAADD(IN pAgreement BIGINT)
BEGIN
		select	cId,
			cAgreement,
			cKey,
			cAssetType as 'cType',
			cAmount,
			cCurrency,
			cLabel,
			'Asset' as cTable 
		from tAgreementAsset
		where cAgreement = pAgreement
	union 
		select	cId,
				cAgreement,
				cKey,
				cDiscountType as 'cType',
				cAmount,
				cCurrency,
				cLabel,
				'Discount' as cTable 
		from tAgreementDiscount
		where cAgreement = pAgreement;

END$$

DELIMITER ;

