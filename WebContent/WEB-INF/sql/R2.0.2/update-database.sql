update tOption set	cEnable=true, 
					cContext='ALBIZIA_CONTEXT',
					cUrl='/servlet/albizia/manager/CostCenterManager',
					cOrder=40
	where ckey='COST_CENTER';
update tOption set cOrder = 30 where ckey='AREA';
update tOption set cEnable = false where ckey='BRANCH';

# Borrar los campos cBranch y cBusinessArea de la tabla tCostCenter
ALTER TABLE tCostCenter DROP FOREIGN KEY CostCenterToBranch;
ALTER TABLE tCostCenter DROP FOREIGN KEY CostCenter_To_BusinessArea;
ALTER TABLE tCostCenter DROP COLUMN cBranch;
ALTER TABLE tCostCenter DROP COLUMN cBusinessArea;

# Agregar la columna(FK) a la tabla tCostCenter para que apunte a la tabla tArea y el campo Key
ALTER TABLE tCostCenter 
ADD COLUMN cArea BIGINT(20) NOT NULL COMMENT 'Area al que pertenece' AFTER cName,
ADD COLUMN cKey VARCHAR(10) NOT NULL UNIQUE COMMENT 'Llave para busquedas posteriores' AFTER cId,
CHANGE COLUMN cName cName VARCHAR(50) NOT NULL;

# Copiar los centros de costo desde la tabla tArea a la tabla de tCostcenter (la tabla tCostCenter debe estar vacia).
DROP TABLE tProfile;
truncate table tCostCenter;
INSERT INTO tCostCenter(cId, cName, cArea, cKey)
	SELECT cId, cCostCenter, cId, cKey FROM tArea order by cId;

# Borrar la columna cCostCenter de la tabla tArea
ALTER TABLE tArea DROP COLUMN cCostCenter;

#Normalizacion de Constrain
ALTER TABLE tEnterpriseConfig
DROP FOREIGN KEY tEnterpriseConfigToEnterprise;
  
ALTER TABLE tEnterpriseConfig
ADD CONSTRAINT tEnterpriseConfigToEnterprise FOREIGN KEY (cEnterprise) REFERENCES tEnterprise(cId);

# Crear un registro en la tabla tEnterprise 
INSERT INTO tEnterprise(cRut, cName, cLegalRep, cRutLegalRep, cCategory, cAddress, cComuna, cPhone, cMutual, cMutualFactor, cCompensationFund)
VALUES('1-9', 'Buildersoft SpA.', 'Claudio Romero', '1-9', 'Servicios profesionales', 'Avenida X, numero 88', 3, '2345678', 1, 0.0231, 1);
SET @e = LAST_INSERT_ID();

# Crear la columna Enterprise en la tabla Area.
ALTER TABLE tArea ADD COLUMN cEnterprise BIGINT(20) NULL;

UPDATE tArea SET cEnterprise = @e;

ALTER TABLE tArea 
CHANGE COLUMN cEnterprise cEnterprise BIGINT(20) NOT NULL,
ADD INDEX IX_tArea_cEnterprise (cEnterprise ASC),
ADD CONSTRAINT FK_tArea_cEnterprise FOREIGN KEY (cEnterprise) REFERENCES tEnterprise(cId);

# Establecer la relacion entre las tablas tArea y tCostCenter
ALTER TABLE tCostCenter
ADD INDEX IX_tCostCenter_cArea (cArea ASC),
ADD CONSTRAINT FK_tCostCenter_cArea FOREIGN KEY (cArea) REFERENCES tArea(cId);

# Borrar la tabla tbusinessarea
drop table tBusinessArea;

RENAME TABLE tPost TO tPosition;
# tPost vs tProfile, borrar la tabla tProfile. Recordar que esta tabla tiene el campo CC y Costo Empresa.
alter table tAgreement 
CHANGE COLUMN cProfile cPosition BIGINT(20) NULL COMMENT 'Id de Cargo, puede ser nulo para cargar empleado desde reloj',
ADD INDEX IX_tAgreement_cPosition(cPosition ASC),
ADD CONSTRAINT FK_tAgreement_cPosition FOREIGN KEY(cPosition) REFERENCES tPosition(cId);

# Cargar la tabla empleados, mover el campo cPost a la tabla tAgreement. Ojo que en la tabla Agreement, el campo se llama cProfile.
INSERT INTO tAgreement(
	cEmployee, 
	cContractType, 
	cStartContract, 
	cEndContract, 
	cPosition, 
	cSalaryRoot, 
	cPFM, 
	cMonthsQuoted, 
	cHealth, 
	cGratificationType, 
	cPaymentType, 
	cBank, 
	cAccountType, 
	cAccountNumber, 
	cCurrencyAccount2, 
	cAmountAccount2, 
	cHorary, 
	cMobilization, 
	cFeeding, 
	cExBoxSystem, 
	cExBoxSystemPRC, 
	cHealthAmount, 
	cHealthCurrency, 
	cAdditionalPFMAmount, 
	cAdditionalPFMCurrency, 
	cSimpleLoads, 
	cDisabilityBurdens, 
	cMaternalLoads, 
	cFamilyAssignmentStretch, cPensionary
)
SELECT cId, 
	(SELECT cId FROM tContractType WHERE cKey = 'UND'),
	NOW(),
	DATE_ADD(NOW(), INTERVAL 90 DAY),
	cPost,
	(SELECT cValue FROM tParameter WHERE cKey = 'BASE_SALARY'),
	(SELECT cId FROM tPFM WHERE cKey = '0'),
	0,
	(SELECT cId FROM tHealth WHERE cKey = '7'),
	1,
	(SELECT cId FROM tPaymentType WHERE cKey = 'EFECTIVO'),
	null, -- Banco
	null, -- account Type
	'', -- cAccountNumber
	null, -- cCurrencyAccount2
	0, -- cAmountAccount2
	1, -- cHorary
	0, -- cMobilization
	0, -- cFeeding
	null, -- cExBoxSystem
	null, -- cExBoxSystemPRC
	0, -- cHealthAmount
	null, -- cHealthCurrency
	0, -- cAdditionalPFMAmount
	null, -- cAdditionalPFMCurrency
	0, -- cSimpleLoads
	0, -- cDisabilityBurdens
	0, -- cMaternalLoads
	(SELECT cId FROM tFamilyAssignmentStretch WHERE cKey = '-'),
	false	
FROM tEmployee;

alter table tEmployee 
drop foreign key EmployeeToPost,
DROP COLUMN cPost;

# Borrar el campo cArea de la tabla empleados.
alter table tEmployee 
drop foreign key EmployeeToArea,
CHANGE COLUMN cArea cCostCenter BIGINT(20) NULL;

alter table tEmployee 
ADD INDEX IX_tEmployee_cCostCenter(cCostCenter ASC),
ADD CONSTRAINT FK_tEmployee_cCostCenter FOREIGN KEY(cCostCenter) REFERENCES tCostCenter(cId);

update tEmployee set cBirthDate=now() where cBirthDate is null;

drop table tBranch;

alter table tAgreement 
CHANGE COLUMN cSalaryRoot cSalaryBase DOUBLE NOT NULL DEFAULT '1'	COMMENT 'Sueldo Base';

#------------------------
ALTER TABLE tBookDiscounts DROP FOREIGN KEY tBookDiscountsToPFMHistory;
ALTER TABLE tPFMHistory DROP FOREIGN KEY PFMHistoryToPeriod;

RENAME TABLE tPFMHistory TO tPFMValue;

alter table tPFMValue
DROP COLUMN cPeriod,
DROP COLUMN cKey,
DROP COLUMN cName,
ADD COLUMN cPFM BIGINT(20) AFTER cId,
ADD INDEX IX_tPFMValue_cPFM (cPFM ASC);

UPDATE tPFMValue SET cPFM=cId;


alter table tPFMValue
CHANGE COLUMN cPFM cPFM BIGINT(20) NOT NULL COMMENT 'Id to tPFM';

alter table tPFMValue
ADD CONSTRAINT FK_tPFMValue_cPFM FOREIGN KEY (cPFM) REFERENCES tPFM(cId);

alter table tPFMValue
ADD COLUMN cUpdated DATETIME NOT NULL COMMENT 'Fecha de actualizacion';

alter table tPFM 
DROP COLUMN cFactor,
DROP COLUMN cSIS;

alter table tBookDiscounts
CHANGE COLUMN cPFMHistory cPFMFactor DOUBLE NOT NULL COMMENT 'Factor of PFM';
#------------------------
ALTER TABLE tBookDiscounts DROP FOREIGN KEY tBookDiscountsToHealthHistory;
ALTER TABLE tHealthHistory DROP FOREIGN KEY HealthHistoryToPeriod;

RENAME TABLE tHealthHistory TO tHealthValue;

alter table tHealthValue
DROP COLUMN cPeriod,
DROP COLUMN cKey,
DROP COLUMN cName,
ADD COLUMN cHealth BIGINT(20) AFTER cId,
ADD INDEX IX_tHealthValue_cHealth (cHealth ASC);

UPDATE tHealthValue SET cHealth=cId;

alter table tHealthValue
CHANGE COLUMN cHealth cHealth BIGINT(20) NOT NULL COMMENT 'Id to tHealth',
ADD COLUMN cUpdated DATETIME NOT NULL COMMENT 'Fecha de actualizacion';

alter table tHealthValue
ADD CONSTRAINT FK_tHealthValue_cHealth FOREIGN KEY (cHealth) REFERENCES tHealth(cId);


alter table tHealth 
DROP COLUMN cFactor;

alter table tBookDiscounts
CHANGE COLUMN cHealthHistory cHealthFactor DOUBLE NOT NULL COMMENT 'Factor de salud, debe ser 7% siempre a menos que cambie en un futuro';

ALTER TABLE tAgreement 
ADD COLUMN cHiredAmount INTEGER AFTER cSalaryBase,
ADD COLUMN cHiredType VARCHAR(1) AFTER cHiredAmount;

UPDATE tAgreement SET cHiredAmount=30, cHiredType='D';

ALTER TABLE tAgreement
CHANGE COLUMN cHiredAmount         cHiredAmount         INTEGER NOT NULL COMMENT 'Cantidad de horas/dias contratados',
CHANGE COLUMN cHiredType           cHiredType           VARCHAR(1) NOT NULL COMMENT 'Puede ser H=Horas o D=Dias',
CHANGE COLUMN cAmountAccount2      cAmountAccount2      DECIMAL(10,2) NULL COMMENT 'Monto de la cuenta 2',
CHANGE COLUMN cSalaryBase          cSalaryBase          DECIMAL(10,2) NOT NULL COMMENT 'Sueldo Base',
CHANGE COLUMN cMobilization        cMobilization        DECIMAL(10,2) NULL COMMENT 'Movilizacion',
CHANGE COLUMN cFeeding             cFeeding             DECIMAL(10,2) NULL COMMENT 'Alimentacion',
CHANGE COLUMN cExBoxSystemPRC      cExBoxSystemPRC      DECIMAL(10,2) NULL COMMENT 'Porcentaje de Caja Ex-Regimen (esto debiera ir a otra tabla al parecer)',
CHANGE COLUMN cHealthAmount        cHealthAmount        DECIMAL(10,2) NOT NULL COMMENT 'Monto sistema de salud',
CHANGE COLUMN cAdditionalPFMAmount cAdditionalPFMAmount DECIMAL(10,2) NULL COMMENT 'Monto de ahorro en AFP';

ALTER TABLE tEnterprise
CHANGE COLUMN cMutualFactor      cMutualFactor      DECIMAL(10,4) NULL COMMENT 'Factor de Mutual, varía de empresa en empresa';


update tOption set cUrl='/servlet/albizia/manager/EnterpriseManager', cContext = 'ALBIZIA_CONTEXT' where cKey='ENTERPRISE_DATA';
update tOption set cUrl='/servlet/config/employee/PositionManager' where ckey='POST';


ALTER TABLE tContractType
CHANGE COLUMN cInsuranceFactorEmployee		cInsuranceFactorEmployee	DECIMAL(10,2) NULL COMMENT 'Factor que aporta el empleado por seguro de cesantía',
CHANGE COLUMN cInsuranceFactorEnterprise	cInsuranceFactorEnterprise	DECIMAL(10,2) NULL COMMENT 'Factor que aporta la empresa';

alter table tAgreement 
DROP FOREIGN KEY AgrementToCurrency,
DROP FOREIGN KEY CurrencyAccount2ToCurrency,
DROP FOREIGN KEY HealthCurrencyToCurrency;

alter table tAgreement 
CHANGE COLUMN cCurrencyAccount2			cCurrencyAccount2		VARCHAR(3) NULL DEFAULT 'PRC' COMMENT 'Moneda de cuanta 2',
CHANGE COLUMN cHealthCurrency			cHealthCurrency			VARCHAR(3) NULL DEFAULT 'PRC' COMMENT 'Moneda de Sistema de Salud',
CHANGE COLUMN cAdditionalPFMCurrency	cAdditionalPFMCurrency	VARCHAR(3) NULL DEFAULT 'PRC' COMMENT 'Moneda Adicional de AFP';

update tAgreement set cCurrencyAccount2='PRC', cHealthCurrency='PRC', cAdditionalPFMCurrency='PRC';

alter table tAgreement 
CHANGE COLUMN cCurrencyAccount2			cCurrencyAccount2			VARCHAR(3) NULL DEFAULT 'PRC' COMMENT 'Moneda de cuanta 2',
CHANGE COLUMN cHealthCurrency			cHealthCurrency				VARCHAR(3) NULL DEFAULT 'PRC' COMMENT 'Moneda de Sistema de Salud',
CHANGE COLUMN cAdditionalPFMCurrency	cAdditionalPFMCurrency		VARCHAR(3) NULL DEFAULT 'PRC' COMMENT 'Moneda Adicional de AFP';

ALTER TABLE tPeriod
CHANGE COLUMN cUF                 cUF                        DECIMAL(8,2) NULL COMMENT 'Valor de la UF para el período',
CHANGE COLUMN cMinSalary          cMinSalary                 DECIMAL(8,2) NULL COMMENT 'Ingreso Mínimo Mensual',
CHANGE COLUMN cLimitGratification cLimitGratificationMonthly DECIMAL(10,2) NULL COMMENT 'Limite gratificacion - 4.75 Ingresos mínimos',
ADD COLUMN cGratificationAmount DECIMAL(10,2) NOT NULL DEFAULT '0' COMMENT 'Monto de gratifcacion que aplica cuando es una gratificacion fija.' AFTER cLimitGratificationMonthly,
ADD COLUMN cEnterpriseLiquidIncome  DECIMAL(12,2) NOT NULL DEFAULT '0' COMMENT 'Suma de todas la utilidades de la empresa para calcular la gratificacion.' AFTER cGratificationAmount,
ADD COLUMN cBonusPay BIT NOT NULL DEFAULT FALSE COMMENT 'Indicador que saber si la empresa paga gratificación en el periodo actual.' AFTER cEnterpriseLiquidIncome,
ADD COLUMN cGratificationFactorEnterprise DECIMAL(5,2) NOT NULL DEFAULT '0' COMMENT 'Factor de gratificacion cuando es una gratificacion anual' AFTER cBonusPay,
ADD COLUMN cTotalRemunerations DECIMAL(12,2) NOT NULL DEFAULT '0' COMMENT 'Total de remuneraciones que percibe la empresa' AFTER cGratificationFactorEnterprise,
ADD COLUMN cUnemploymentLimit DECIMAL(5,2) NOT NULL DEFAULT '0' COMMENT 'Limite en UF para seguro de desempleo' AFTER cTotalRemunerations,
ADD COLUMN cAccidentInsuranceFactor DECIMAL(5,2) NOT NULL DEFAULT '0' COMMENT 'Factor de seguro de accidente' AFTER cUnemploymentLimit,
DROP COLUMN cGratificationFactor,
DROP COLUMN cLimitIPS,
DROP COLUMN cLimitInsurance,
DROP COLUMN cLimitHealth,
DROP COLUMN cUTM,
DROP COLUMN cDaysForYear,
DROP COLUMN cOvertimeFactor;

ALTER TABLE tR_AgreementAPV
drop foreign key r_AgreementAPVToCurrency,
CHANGE COLUMN cCurrency cCurrency ENUM('CLP', 'CLF','PERCENT');

ALTER TABLE tPFMValue
CHANGE COLUMN cFactor cFactor DECIMAL(5,2) COMMENT 'Factor que descuenta la AFP',
CHANGE COLUMN cSIS    cSIS    DECIMAL(5,2),
CHANGE COLUMN cUpdated cUpdated DATE NOT NULL COMMENT 'Fecha de actualizacion';

ALTER TABLE tHealthValue 
CHANGE cFactor cAmount decimal(5,2) NOT NULL;
ALTER TABLE tHealthValue 
MODIFY COLUMN cAmount decimal(5,2) NOT NULL;
ALTER TABLE tHealthValue
ADD COLUMN cCurrency ENUM('CLP', 'CLF','PERCENT') default 'PERCENT' after cAmount;

ALTER TABLE tGratificationType ADD COLUMN cKey varchar(7) NULL AFTER cId;
update tGratificationType set cKey = 'NONE' where cid=1;
update tGratificationType set cKey = 'MONTHLY' where cid=2;
update tGratificationType set cKey = 'OLDER' where cid=3;
insert into tGratificationType(cId, cKey, cName) VALUES(4,'FIXED','Valor fijo');
ALTER TABLE tGratificationType MODIFY COLUMN cKey varchar(7) NOT NULL;

ALTER TABLE tEnterpriseConfig 
ADD COLUMN cGratificationType BIGINT(20) NOT NULL DEFAULT '1' AFTER cEnterprise,
ADD INDEX IX_tEnterpriseConfig_cGratificationType (cGratificationType ASC),
ADD CONSTRAINT FK_tEnterpriseConfig_cGratificationType FOREIGN KEY (cGratificationType) REFERENCES tGratificationType(cId);

CREATE TABLE tSettlement (
  cId bigint(20) NOT NULL AUTO_INCREMENT,
  cPeriod bigint(20) NOT NULL COMMENT 'Enlace con la tabla tPeriod - 01/Mes/Año',
  cAgreement bigint(20) NOT NULL COMMENT 'Contrato, éste se asocia con el empleado - tAgreement.cId',
  cBaseSalary decimal(10,2) NOT NULL DEFAULT '0' COMMENT 'Sueldo Base - cp from tAgreement.cSalaryRoot',
  cWorkedAmount int(11) NOT NULL DEFAULT '30' COMMENT 'Horas o días trabajados, segun WorkedType',
  cWorkedType ENUM('H', 'D') NOT NULL DEFAULT 'D' COMMENT 'Indica tipo de tiempo trabajado D/H',
  cOvertimeTime int(11) NOT NULL DEFAULT '0' COMMENT 'Cantidad Horas Extras',
  cOvertimeAmount decimal(10,2) NOT NULL DEFAULT '0' COMMENT 'Monto por concepto de horas extras',
  cPensionary bit(1) NOT NULL DEFAULT b'0' COMMENT 'Indica si es pensionado',
  cPFMValue bigint(20) NOT NULL COMMENT 'Indicador de APF',
  cHealthValue bigint(20) NOT NULL COMMENT 'Indicador de Sistema de Salud',
  cUnemploymentLimit decimal(5,2) NOT NULL DEFAULT '0' COMMENT 'Limite del seguro de cesantía',
  cTotalAssignment decimal(10,2) NOT NULL DEFAULT '0' COMMENT 'Total de asignaciones',
  cTaxableSalary decimal(10,2) NOT NULL DEFAULT '0' COMMENT 'Total Imponible',
  cReachedSalary decimal(10,2) NOT NULL DEFAULT '0' COMMENT 'Alcance Líquido',
  cLiquidSalary decimal(10,2) NOT NULL DEFAULT '0' COMMENT 'Alcance Líquido',
  PRIMARY KEY (cId),
  KEY IX_tSettlement_cPeriod (cPeriod),
  KEY IX_tSettlement_cAgreement (cAgreement),
  KEY IX_tSettlement_cPFMValue (cPFMValue),
  KEY IX_tSettlement_cHealthValue (cHealthValue),
  CONSTRAINT FK_tSettlement_cPeriod FOREIGN KEY (cPeriod) REFERENCES tPeriod (cId),
  CONSTRAINT FK_tSettlement_cAgreement FOREIGN KEY (cAgreement) REFERENCES tAgreement (cId),
  CONSTRAINT FK_tSettlement_cPFMValue FOREIGN KEY (cPFMValue) REFERENCES tPFMValue (cId),
  CONSTRAINT FK_tSettlement_cHealthValue FOREIGN KEY (cHealthValue) REFERENCES tHealthValue (cId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#---------------------------------------
CREATE TABLE tAgreementAsset (
	cId BIGINT(20) NOT NULL AUTO_INCREMENT,
	cAgreement bigint(20) NOT NULL,
	cKey VARCHAR(15) NOT NULL COMMENT 'Identificador para que el programa realice la evaluacion de cálculos',
	cAssetType ENUM('ASSIGNMENT', 'NORMAL') DEFAULT 'NORMAL' NOT NULL COMMENT 'Tipo del abono, puede ser normal o asignacion.',
	cAmount NUMERIC(10,4) DEFAULT '0' NOT NULL COMMENT 'Monto del abono',
	cCurrency ENUM('CLP', 'CLF', 'PERCENT') DEFAULT 'CLP' NOT NULL,
	cLabel varchar(100) NULL COMMENT 'Nombre humano.',
	CONSTRAINT PK_tAgreementAsset_cId PRIMARY KEY (cId),
	CONSTRAINT FK_tAgreementAsset_cId FOREIGN KEY (cId) REFERENCES tAgreement(cId)
)
ENGINE=InnoDB;
CREATE INDEX IX_tAgreementAsset_cAgreement ON tAgreementAsset (cAgreement);

CREATE TABLE tAgreementDiscount (
	cId BIGINT(20) NOT NULL AUTO_INCREMENT,
	cAgreement bigint(20) NOT NULL,
	cKey VARCHAR(10) NOT NULL COMMENT 'Identificador para que el programa realice la evaluacion de cálculos',
	cDiscountType ENUM('LEGAL', 'PREVISIONAL', 'TAX', 'JUDICIAL', 'AFTER_REACHED_LIQUID', 'COMPANY_PAID') DEFAULT 'AFTER_REACHED_LIQUID' NOT NULL COMMENT 'Tipo del abono, puede alguno de los enumerados',
	cAmount NUMERIC(10,4) DEFAULT '0' NOT NULL COMMENT 'Monto del Descuento',
	cCurrency ENUM('CLP', 'CLF', 'PERCENT') DEFAULT 'CLP' NOT NULL,
	cLabel varchar(100) NULL COMMENT 'Nombre humano.',
	CONSTRAINT PK_tAgreementDiscount_cId PRIMARY KEY (cId),
	CONSTRAINT FK_tAgreementDiscount_cId FOREIGN KEY (cId) REFERENCES tAgreement(cId)
)
ENGINE=InnoDB;
CREATE INDEX IX_tAgreementDiscount_cAgreement ON tAgreementDiscount (cAgreement);

DROP TABLE tAssetDiscountValue;
DROP TABLE tAssetDiscount;     
DROP TABLE tAssetDiscountType; 

#------------------------------
CREATE TABLE tAADDSettlement (
  cId bigint(20) NOT NULL AUTO_INCREMENT,
  cSettlement bigint(20) NOT NULL COMMENT 'Id del contrato',
  cAADDKey bigint(20) NOT NULL COMMENT 'Tipo de habono o descuento',
  cCurrency ENUM('CLP', 'CLF','PERCENT') NOT NULL DEFAULT 'CLP',
  cAmount decimal(10,2) NOT NULL DEFAULT '0',
  cName varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (cId),
  INDEX IX_tAADDSettlement_cSettlement (cSettlement ASC),
  CONSTRAINT FK_tAADDSettlement_cSettlement FOREIGN KEY (cSettlement) REFERENCES tSettlement(cId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#------------------------------

ALTER TABLE tAgreement
CHANGE COLUMN cCurrencyAccount2 cCurrencyAccount2 varchar(7);

update tAgreement set cCurrencyAccount2 = 'PERCENT' where cCurrencyAccount2='PRC';

ALTER TABLE tAgreement
CHANGE COLUMN cCurrencyAccount2 cAccount2Currency ENUM('CLP', 'CLF','PERCENT') NOT NULL DEFAULT 'CLP' COMMENT 'Moneda de cuanta 2',
CHANGE COLUMN cAmountAccount2 cAccount2Amount decimal(10,2) NOT NULL COMMENT 'Monto de la cuenta 2';

DROP TABLE tUniqueTax;

CREATE TABLE tUniqueTax (
  cId bigint(20) NOT NULL AUTO_INCREMENT,
  cPeriod bigint(20) NOT NULL COMMENT 'Periodo, que corresopnde a algun mes',
  cFrom decimal(10,2) NULL COMMENT 'Inicio del tramo',
  cTo decimal(10,2) NULL COMMENT 'Término del tramo',
  cFactor decimal(6,4) COMMENT 'Factor de descuento',
  cUnlimited bit COMMENT 'Indicador que indica sin limite superior',
  cAmount decimal(10,2) COMMENT 'Cantidad a rebajar',
  PRIMARY KEY (cId),
  INDEX IX_tUniquetax_cPeriod (cPeriod ASC),
  CONSTRAINT FK_tUniquetax_cPeriod FOREIGN KEY (cPeriod) REFERENCES tPeriod(cId)
) ENGINE=InnoDB;

ALTER TABLE tPeriod
ADD COLUMN cUniqueTaxLimitAmount   DECIMAL(5,2) NOT NULL DEFAULT '60' COMMENT 'Monto del limite de impuesto' AFTER cAccidentInsuranceFactor,
ADD COLUMN cUniqueTaxLimitCurrency ENUM('CLP', 'CLF','PERCENT') NOT NULL DEFAULT 'CLF' COMMENT 'Moneda de limite de impuesto' AFTER cUniqueTaxLimitAmount;


CREATE TABLE tHoursWorked (
  cId bigint(20) NOT NULL AUTO_INCREMENT,
  cPeriod bigint(20) NOT NULL COMMENT 'Periodo, tabla tPeriod',
  cEmployee bigint(20) NOT NULL COMMENT 'Empleado',
  cAmount decimal(5,2) COMMENT 'Cantidad de horas',
  PRIMARY KEY (cId),
  INDEX IX_tHoursWorked_cPeriod (cPeriod ASC),
  INDEX IX_tHoursWorked_cEmployee (cEmployee ASC),
  CONSTRAINT FK_tHoursWorked_cPeriod FOREIGN KEY (cPeriod) REFERENCES tPeriod(cId),
  CONSTRAINT FK_tHoursWorked_cEmployee FOREIGN KEY (cEmployee) REFERENCES tEmployee(cId)
) ENGINE=InnoDB;

INSERT INTO tOption (cKey,cLabel,cContext,cUrl,cParent,cType,cOrder,cEnable,cIsAdmin) 
VALUES ('HOURS_WORKED','Horas Trabajadas','ALBIZIA_CONTEXT','/servlet/process/employee/ReadHoursWorked',8,1,55,1,false);

UPDATE tOption SET cUrl='/servlet/manager/period/PeriodManager' WHERE cKey='PERIODS';

drop trigger InsertOnPeriod;

ALTER TABLE tAgreement DROP FOREIGN KEY AgreementToGratificationType ;
ALTER TABLE tAgreement DROP INDEX gratificationType_index;
ALTER TABLE tAgreement DROP COLUMN cGratificationType;

ALTER TABLE tBook DROP FOREIGN KEY BookToHorary ;
ALTER TABLE tAgreement DROP FOREIGN KEY AgreementToHorary ;
DROP TABLE tHorary;

ALTER TABLE tAgreementDiscount DROP FOREIGN KEY FK_tAgreementDiscount_cId,
ADD CONSTRAINT FK_tAgreementDiscount_cAgreement FOREIGN KEY (cAgreement) REFERENCES tAgreement(cId);

ALTER TABLE tAgreementAsset DROP FOREIGN KEY FK_tAgreementAsset_cId,
ADD CONSTRAINT FK_tAgreementAsset_cAgreement FOREIGN KEY (cAgreement) REFERENCES tAgreement(cId);

ALTER TABLE tAADDSettlement
CHANGE COLUMN	cAADDKey cAADDKey varchar(20) NOT NULL COMMENT 'Tipo de habono o descuento',
ADD COLUMN 		cType ENUM('A','D') NOT NULL COMMENT 'Asset or Dicount' AFTER cSettlement;


ALTER TABLE tBook 
DROP FOREIGN KEY BookToEmployee,
DROP FOREIGN KEY BookToPeriod;

DROP TABLE tBookAssets;
DROP TABLE tBookDiscounts;
DROP TABLE tBook;

#ALTER TABLE tAADDSettlement MODIFY COLUMN cLabel varchar(100) DEFAULT '' NOT NULL ;
ALTER TABLE tAADDSettlement CHANGE cName cLabel varchar(100) DEFAULT '' NOT NULL ;

DROP PROCEDURE if exists pGetAssetDiscount;
ALTER TABLE tAgreement DROP COLUMN cHorary;



#update toption set cUrl=null where ckey='PFM';
INSERT INTO tOption (cKey,cLabel,cContext,cUrl,cParent,cType,cOrder,cEnable,cIsAdmin) 
	VALUES ('PFM-LIST','Lista','ALBIZIA_CONTEXT','/servlet/config/pfm/PFMListManager',54,1,10,true,false);
INSERT INTO tOption (cKey,cLabel,cContext,cUrl,cParent,cType,cOrder,cEnable,cIsAdmin) 
	VALUES ('PFM-VALUES','Valores','ALBIZIA_CONTEXT','/servlet/config/pfm/PFMValueManager',54,1,20,true,false);

insert into tUniqueTax(cPeriod, cFrom, cTo, cFactor, cUnlimited, cAmount) values(1, 0, 465696, 0, false, 0);
insert into tUniqueTax(cPeriod, cFrom, cTo, cFactor, cUnlimited, cAmount) values(1, 465696.01, 1034880, 0.05, false, 23284.8);
insert into tUniqueTax(cPeriod, cFrom, cTo, cFactor, cUnlimited, cAmount) values(1, 1034880.01, 1724800, 0.10,	false, 75028.8);
insert into tUniqueTax(cPeriod, cFrom, cTo, cFactor, cUnlimited, cAmount) values(1, 1724800.01, 2414720, 0.15, false, 161268.8);
insert into tUniqueTax(cPeriod, cFrom, cTo, cFactor, cUnlimited, cAmount) values(1, 2414720.01, 3104640, 0.25, false, 402740.8);
insert into tUniqueTax(cPeriod, cFrom, cTo, cFactor, cUnlimited, cAmount) values(1, 3104640.01, 4139520, 0.32, false, 620065.6);
insert into tUniqueTax(cPeriod, cFrom, cTo, cFactor, cUnlimited, cAmount) values(1, 4139520.01, 5174400, 0.37, false, 827041.6);
insert into tUniqueTax(cPeriod, cFrom, cTo, cFactor, cUnlimited, cAmount) values(1, 5174400.01, 0, 0.4, true, 982273.6);

ALTER TABLE tSettlement 
CHANGE cAgreement cAgreementSettlement bigint(20) NOT NULL COMMENT 'Contrato del momento del calculo' ;

ALTER TABLE tSettlement DROP FOREIGN KEY FK_tSettlement_cAgreement;

CREATE TABLE tAgreementSettlement (
  cId bigint(20) NOT NULL AUTO_INCREMENT,
  cEmployee bigint(20) NOT NULL COMMENT 'Id del empleado',
  cContractType bigint(20) NOT NULL COMMENT 'Tipo de contrato: fijo o indefinido',
  cStartContract date NOT NULL COMMENT 'Inicio de contrato',
  cEndContract date DEFAULT NULL COMMENT 'Fin de contrato',
  cPosition bigint(20) DEFAULT NULL COMMENT 'Id de Cargo, puede ser nulo para cargar empleado desde reloj',
  cSalaryBase decimal(10,2) NOT NULL COMMENT 'Sueldo Base',
  cHiredAmount int(11) NOT NULL COMMENT 'Cantidad de horas/dias contratados',
  cHiredType varchar(1) NOT NULL COMMENT 'Puede ser H=Horas o D=Dias',
  cPFM bigint(20) NOT NULL COMMENT 'AFP',
  cMonthsQuoted int(11) NOT NULL DEFAULT '0' COMMENT 'Meses cotizados, esto se usa para el calculo de vacaciones progresivas',
  cHealth bigint(20) NOT NULL COMMENT 'Sistema de salud, isapre o fonasa',
  cPaymentType bigint(20) NOT NULL COMMENT 'Tipo de pago',
  cBank bigint(20) DEFAULT NULL COMMENT 'Banco para efectos de pago de sueldo',
  cAccountType bigint(20) DEFAULT NULL COMMENT 'Tipo de cuenta a la cual se hará el deposito',
  cAccountNumber varchar(20) NOT NULL DEFAULT '' COMMENT 'Número de la cuenta a la cual se le hará el deposito',
  cAccount2Currency enum('CLP','CLF','PERCENT') NOT NULL DEFAULT 'CLP' COMMENT 'Moneda de cuanta 2',
  cAccount2Amount decimal(10,2) NOT NULL COMMENT 'Monto de la cuenta 2',
  cMobilization decimal(10,2) DEFAULT NULL COMMENT 'Movilizacion',
  cFeeding decimal(10,2) DEFAULT NULL COMMENT 'Alimentacion',
  cExBoxSystem bigint(20) DEFAULT NULL COMMENT 'Caja Ex-Regimen',
  cExBoxSystemPRC decimal(10,2) DEFAULT NULL COMMENT 'Porcentaje de Caja Ex-Regimen (esto debiera ir a otra tabla al parecer)',
  cHealthAmount decimal(10,2) NOT NULL COMMENT 'Monto sistema de salud',
  cHealthCurrency varchar(3) DEFAULT 'PRC' COMMENT 'Moneda de Sistema de Salud',
  cAdditionalPFMAmount decimal(10,2) DEFAULT NULL COMMENT 'Monto de ahorro en AFP',
  cAdditionalPFMCurrency varchar(3) DEFAULT 'PRC' COMMENT 'Moneda Adicional de AFP',
  cSimpleLoads int(11) NOT NULL DEFAULT '0' COMMENT 'Cargas Simples',
  cDisabilityBurdens int(11) NOT NULL DEFAULT '0' COMMENT 'Cargas Invalidez',
  cMaternalLoads int(11) NOT NULL DEFAULT '0' COMMENT 'Cargas Maternales',
  cFamilyAssignmentStretch bigint(20) NOT NULL COMMENT 'Tramo de asignación familiar',
  cPensionary bit(1) NOT NULL DEFAULT b'0' COMMENT 'Indica si es pensionado',
  PRIMARY KEY (cId)
) ENGINE=InnoDB;

ALTER TABLE tSettlement 
ADD INDEX IX_tSettlement_cAgreementSettlement (cAgreementSettlement ASC),
ADD CONSTRAINT FK_tSettlement_cAgreementSettlement FOREIGN KEY (cAgreementSettlement) REFERENCES tAgreementSettlement(cId);

ALTER TABLE tSettlement ADD cContractType BIGINT(20) NOT NULL COMMENT 'Tipo de contrato: fijo o indefinido';



ALTER TABLE tAgreement MODIFY COLUMN cAccount2Currency enum('CLP','CLF','PRC', 'PERCENT') DEFAULT 'PRC' NULL COMMENT 'Moneda Adicional de AFP' ;

update tAgreement set cAccount2Currency='PRC' where cAccount2Currency='PERCENT';

ALTER TABLE tAgreement MODIFY COLUMN cAccount2Currency enum('CLP','CLF','PRC') 		DEFAULT 'CLP' NOT NULL COMMENT 'Moneda de cuanta 2' ;
ALTER TABLE tAgreement MODIFY COLUMN cAdditionalPFMCurrency enum('CLP','CLF','PRC') DEFAULT 'PRC' NULL COMMENT 'Moneda Adicional de AFP' ;


update bsframework.tConfig set cValue='https://s3.us-east-2.amazonaws.com/www.dalea.cl' where cKey = 'STATIC_CONTEXT';


UPDATE tVersion SET cVersion='2.0.2', cUpdated=NOW() WHERE cKey = 'DBT';

#RENAME TABLE tAssetDiscount TO tAADDAgreement;
#RENAME TABLE tAssetDiscountValue TO tAADDAgreementValue;
#RENAME TABLE tAssetDiscountType TO tAADDType;
#
#
#ALTER TABLE tAADDAgreement
#DROP COLUMN cEnable,
#DROP COLUMN cLimit,
#DROP COLUMN cIndex,
#DROP FOREIGN KEY tAssetDiscountToAssetDiscountType,
#CHANGE cAssetDiscountType cAADDType bigint(20) NOT NULL COMMENT 'Tipo de habono o descuento';
#
#ALTER TABLE tAADDAgreement
#DROP INDEX AssetDiscount_index_AssetDiscountType,
#ADD INDEX IX_tAADDAgreement_cAADDType (cAADDType ASC),
#ADD CONSTRAINT FK_tAADDAgreement_cAADDType FOREIGN KEY (cAADDType) REFERENCES tAADDType(cId);
#
#ALTER TABLE tAADDAgreementValue
#CHANGE cBook cAADDAgreement bigint(20) NOT NULL COMMENT 'Referencia a tabla tAgreement';
#
#ALTER TABLE tAADDAgreementValue
#DROP FOREIGN KEY tAssetDiscountValueToAssetDiscount,
#ADD INDEX IX_tAADDAgreementValue_cAADDAgreement (cAADDAgreement ASC),
#ADD CONSTRAINT FK_tAADDAgreementValue_cAADDAgreement FOREIGN KEY (cAADDAgreement) REFERENCES tAADDAgreement(cId),
#DROP COLUMN cAssetDiscount,
#ADD COLUMN cCurrency ENUM('CLP', 'CLF','PERCENT') NOT NULL DEFAULT 'CLP' AFTER cValue,
#DROP FOREIGN KEY tAssetDiscountValueToBook,
#ADD COLUMN cAgreement bigint(20) NOT NULL AFTER cId,
#ADD INDEX IX_tAADDAgreementValue_cAgreement (cAgreement ASC),
#ADD CONSTRAINT FK_tAADDAgreementValue_cAgreement FOREIGN KEY (cAgreement) REFERENCES tAgreement(cId);
#
#UPDATE tAADDType SET cKey='A',cName='Abonos' WHERE cKey = 'BON';
#UPDATE tAADDType SET cKey='D' WHERE cKey = 'DES';
#
#ALTER TABLE tAADDType
#ADD COLUMN cType ENUM('ASSIGNMENT', 'NORMAL', 'LEGAL', 'PREVISIONAL', 'TAX', 'JUDICIAL', 'AFTER_REACHED_LIQUID', 'COMPANY_PAID') NULL AFTER cKey COMMENT 'ASSIGNMENT and NORMAL=Asset, others for Dicount',
#CHANGE cKey cKey ENUM('A', 'D') NOT NULL COMMENT 'A=Haberes, D=Descuentos',
#drop index cKey;
#
#UPDATE tAADDType SET cType='NORMAL' WHERE cKey = 'A';
#UPDATE tAADDType SET cType='AFTER_REACHED_LIQUID' WHERE cKey = 'D';


