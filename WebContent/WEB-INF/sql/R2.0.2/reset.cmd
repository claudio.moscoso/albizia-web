rem @echo off
cls
set CurrDir=%cd%
rem echo %CurrDir%

rem %cd:~0,3% -> current drive

cd %GDRIVE_PATH%\timecontrol-install\RespaldoDB\desarrollo
%GDRIVE_PATH:~0,2%
call restore-backup.cmd %1 1.3.3

cd %CurrDir%
%CurrDir:~0,2%

call run-once %1

