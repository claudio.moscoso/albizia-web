DROP PROCEDURE if exists pGetEnterpriseConfigOfEmployee;
DELIMITER $$

CREATE PROCEDURE pGetEnterpriseConfigOfEmployee(IN pEmployee BIGINT)
BEGIN
	select	e.cId,
			e.cEnterprise,
			e.cGratificationType,
			e.cShowDateUfUtm,
			e.cShowProfile,
			e.cShowCostCenter,
			e.cShowDataAgreement,
			e.cShowSalaryRoot,
			e.cShowEmployerBonus,
			e.cShowWorkDay,
			e.cShowNetPaymentScope,
			e.cTextFootSalary,
			e.cMailNotice,
			e.cViewLastSettlements
	from tEmployee as a
	left join tCostCenter as b on a.cCostCenter = b.cId
	left join tArea as c on b.cArea = c.cId
	left join tEnterprise as d on c.cEnterprise = d.cId
	left join tEnterpriseConfig as e on e.cEnterprise = d.cId
	where a.cid=pEmployee;
			
END$$

DELIMITER ;

