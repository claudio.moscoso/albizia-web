DROP PROCEDURE if exists pGetEmployeeHoursWorked;
DELIMITER $$

CREATE PROCEDURE pGetEmployeeHoursWorked(IN pPeriod BIGINT, IN pEmployee BIGINT)
BEGIN
	SELECT b.cId, a.cId as cEmployeeId, a.cRut, a.cName, IFNULL(b.cAmount, 0) AS cAmount
	FROM tEmployee as a
	LEFT JOIN tHoursWorked as b on a.cId = b.cEmployee AND b.cPeriod = pPeriod
	WHERE a.cEnabled = true and a.cId = pEmployee;

END$$

DELIMITER ;

