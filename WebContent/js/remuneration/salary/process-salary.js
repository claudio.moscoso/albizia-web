var t = null;
var style = "success";

function onLoadPage() {
	var lang = {
		"lengthMenu" : "Mostrando _MENU_ registros por página",
		"zeroRecords" : "Nada encontrado",
		"info" : "Página _PAGE_/_PAGES_ - _TOTAL_ registros ",
		"infoEmpty" : "Sin información",
		"infoFiltered" : "(filtro aplicado)",
		"search" : "Buscar:",
		"paginate" : {
			"first" : "Primera",
			"last" : "Última",
			"next" : "Siguiente",
			"previous" : "Anterior"
		}
	};

	var config = {
		"language" : lang,
		"paging" : true,
		"info" : true,
		"ordering" : false,
		"searching" : false,
		"stateSave" : true,
		// "dom" : '<ftip>',
		// "dom" : '<lftip>',
		"pagingType" : "full_numbers",
		"scrollY" : "300px",
		"scrollCollapse" : true,
		"lengthMenu" : [ [ -1, 10, 50, 100, 300 ],
				[ "Todos", 10, 50, 100, 300 ] ],
	};

	t = $('#TableSettlement').DataTable(config);
	t.on('click', 'tr', function() {
		$(this).toggleClass(style);

		/*
		 * if ( $(this).hasClass(style) ) { $(this).removeClass(style); } else {
		 * t.$('tr.'+style).removeClass(style); $(this).addClass(style); }
		 */
	});
}

function doCalculate() {
	var fs = t.rows('.' + style).data();
	var x = '';
	// alert(fs.length);

	for (var i = 0; i < fs.length; i++) {
//		x += fs[i][0] + " - ";
		$("#CalculateSalary").prepend(
				"<input type='hidden' value='" + fs[i][0] + "' name='cId'>");
	}
	// alert(x);
	if (fs.length > 0) {
		document.getElementById('CalculateSalary').submit();
	}
}
