var currentTab = 'ASSET';
var isAdding = false;

function onLoadPage(){
//	clickTab(focus.toUpperCase());
}

function newRow() {
	// var tableName = getTableName();
	var row = null;
	var cell = null;
	var isAsset = (currentTab == 'ASSET');
	//
	// if (isAsset) {
	// tableName = 'asset-table';
	// } else {
	// tableName = 'discount-table';
	// }
	var table = getTable(); // document.getElementById(tableName);
	row = table.insertRow();
	// row.style.display = 'none';

	cell = row.insertCell();
	cell.innerHTML = document.getElementById(isAsset ? "AssetTypeContainer"
			: "DiscountTypeContainer").innerHTML;

	cell = row.insertCell();
	cell.innerHTML = "<input name='Key' class='form-control'>";

	cell = row.insertCell();
	cell.innerHTML = "<input name='Name' class='form-control'>";

	cell = row.insertCell();
	cell.innerHTML = document.getElementById("AssetCurrencyContainer").innerHTML;

	cell = row.insertCell();
	cell.innerHTML = "<input name='Amount' id='Amount' class='form-control' value='0' "
			+ "onfocus='javascript:doubleFocus(this);' "
			+ "onblur='javascript:doubleBlur(this);'>";

	buttons(false);
	isAdding = true;

	// $(row).show(speed);
	// $('#newButton').hide(speed);
	// $('#acceptButton').show(speed);
	// $('#cancelButton').show(speed);
}

function getTable() {
	var isAsset = (currentTab == 'ASSET');

	if (isAsset) {
		tableName = 'asset-table';
	} else {
		tableName = 'discount-table';
	}
	return document.getElementById(tableName);
}

function clickTab(clicked) {
//	alert(clicked);
	if (isAdding) {
		cancelNew();
	}

	currentTab = clicked;

	buttons(true);

//	console.log("isAdding: " + isAdding);

	$("#AssetDiscount").val(clicked);
}

function buttons(asDefault) {
	if (asDefault) {
		$('#newButton').show(speed);
		$('#acceptButton').hide(speed);
		$('#cancelButton').hide(speed);
	} else {
		$('#newButton').hide(speed);
		$('#acceptButton').show(speed);
		$('#cancelButton').show(speed);
	}
}

function cancelNew() {
	var table = getTable();
	// var lastRow = table.rows[table.rows.length-1];
	// $(lastRow).hide(speed);
	// alert(lastRow.innerHTML);
	buttons(true);
	table.deleteRow(-1);
	isAdding = false;

}

function deleteRow(id, type) {
	if (confirm('¿Esta seguro de eliminar este concepto?')) {
		document.getElementById("cId").value = id;
		document.getElementById("cType").value = type;
		$('#DeleteForm').submit();
	}

}