function onLoadPage() {
	// alert(1);
	var lang = {
		"lengthMenu" : "Mostrando _MENU_ registros por página",
		"zeroRecords" : "Nada encontrado",
		"info" : "Página _PAGE_/_PAGES_ - _TOTAL_ registros ",
		"infoEmpty" : "Sin información",
		"infoFiltered" : "(filtro aplicado)",
		"search" : "Buscar:",
		"paginate" : {
			"first" : "Primera",
			"last" : "Última",
			"next" : "Siguiente",
			"previous" : "Anterior"
		}
	};
	var config = {
		"language" : lang,
		"paging" : true,
		"info" : true,
		"ordering" : false,
		"stateSave" : false,
		"dom" : '<ftip>',
		// "dom" : '<lftip>',
		"pagingType" : "full_numbers",
		"scrollX" : "100%",
		"scrollCollapse" : false
	};
	$('#DataTest').DataTable(config);

	// alert(2);
}