var t = null;
var style = "success";
function onLoadPage() {
	/*
	 * try { $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
	 * var filtred = $("#OfficeFilter").val();
	 * 
	 * if (filtred == data[2] || filtred == '') { return true; } else { return
	 * false; }
	 * 
	 * }); } catch (e) { alert(e); }
	 */

	var lang = {
		"lengthMenu" : "Mostrando _MENU_ registros por página",
		"zeroRecords" : "Nada encontrado",
		"info" : "Página _PAGE_/_PAGES_ - _TOTAL_ registros ",
		"infoEmpty" : "Sin información",
		"infoFiltered" : "(filtro aplicado)",
		"search" : "Buscar:",
		"paginate" : {
			"first" : "Primera",
			"last" : "Última",
			"next" : "Siguiente",
			"previous" : "Anterior"
		}
	};
	var config = {
		"language" : lang,
		"paging" : true,
		"info" : true,
		"ordering" : false,
		"stateSave" : true,
		// "dom" : '<ftip>',
		// "dom" : '<lftip>',
		"pagingType" : "full_numbers",
		"scrollX" : "300px",
		"scrollCollapse" : false,
		"lengthMenu" : [ [ -1, 10, 50, 100, 200 ],
				[ "Todos", 10, 50, 100, 200 ] ],
	};
	t = $('#DataTable').DataTable(config);

	
	t.on('click', 'tr', function() {
		$(this).toggleClass(style);

		var fs = t.rows('.' + style);

		if (fs.data().length == 0) {
			// $('#changeHoursButton').toggleClass('disabled');
			$('#changeHoursButton').prop('disabled', true);
		} else {
			$('#changeHoursButton').prop('disabled', false);
		}
	});
	/**
	 * $('#OfficeFilter').on('change', function() { try { t.draw(); } catch (e) {
	 * alert(e); } });
	 */
}

function doAction() {
	var fs = t.rows('.' + style).data();
//	alert(fs.length);
	var url = contextPath + "/servlet/process/employee/LoadEmployeesInfo";
//	var url = "/dalea-web/servlet/ShowParameters";
	for (var i = 0; i < fs.length; i++) {
		$("#form").prepend(
				"<input type='hidden' value='" + fs[i][0] + "' name='cId'>");
	}
//	alert($('#form').html());
//	alert(url);
	try {
		$('#form').prop("action", url);
		$('#form').submit();
	} catch (e) {
		alert(e);
	}
}
