/**
 * $.extend( true, $.fn.dataTable.defaults, { "searching": false, "ordering":
 * false } );
 */
var t = null;

function onLoadPage() {
	try {
		$.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
			var filtred = $("#OfficeFilter").val();

			if (filtred == data[2] || filtred == '') {
				return true;
			} else {
				return false;
			}

		});
	} catch (e) {
		alert(e);
	}
	// $('#frm').hide();
	// alert($('#frm').html() );
	/**
	 * var langu = { "decimal": ",", "thousands": "."}; var config = {
	 * "language": langu, "paging": true, "info": true, "stateSave": false,
	 * "pagingType": "full_numbers" }; $('#DataTest').DataTable(config);
	 */
	var lang = {
		"lengthMenu" : "Mostrando _MENU_ registros por página",
		"zeroRecords" : "Nada encontrado",
		"info" : "Página _PAGE_/_PAGES_ - _TOTAL_ registros ",
		"infoEmpty" : "Sin información",
		"infoFiltered" : "(filtro aplicado)",
		"search" : "Buscar:",
		"paginate" : {
			"first" : "Primera",
			"last" : "Última",
			"next" : "Siguiente",
			"previous" : "Anterior"
		}
	};
	var config = {
		"language" : lang,
		"paging" : true,
		"info" : true,
		"ordering" : false,
		"stateSave" : true,
		// "dom" : '<ftip>',
		// "dom" : '<lftip>',
		"pagingType" : "full_numbers",
		"scrollY" : "300px",
		"scrollCollapse" : true,
		"lengthMenu" : [ [ -1, 10, 50, 100, 300 ],
				[ "Todos", 10, 50, 100, 300 ] ],
	};
	t = $('#DataTest').DataTable(config);
	var style = "success";
	// var style = "selected";
	t.on('click', 'tr', function() {
		$(this).toggleClass(style);

		/*
		 * if ( $(this).hasClass(style) ) { $(this).removeClass(style); } else {
		 * t.$('tr.'+style).removeClass(style); $(this).addClass(style); }
		 */
	});

	$('#OfficeFilter').on('change', function() {
		// alert(t);
		try {
			t.draw();
		} catch (e) {
			alert(e);
		}
	});

}

