package cl.buildersoft.albizia.web.servlet.process;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.business.dto.EmployeeListDTO;
import cl.buildersoft.dalea.bean.Enterprise;
import cl.buildersoft.dalea.bean.Period;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.database.BSmySQL;
import cl.buildersoft.framework.exception.BSDataBaseException;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.util.BSUtils;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/remuneration/process/salary/ProcessSalary")
public class ProcessSalary extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(ProcessSalary.class);
	private static final long serialVersionUID = 8964108846441089172L;

	public ProcessSalary() {
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		BSmySQL mysql = new BSmySQL();
		BSConnectionFactory cf = new BSConnectionFactory();

		Connection conn = cf.getConnection(request);

		try {
			BSBeanUtils bu = new BSBeanUtils();
			Enterprise enterprise = new Enterprise();

			Long periodId = Long.parseLong(mysql.callFunction(conn, "fGetOpenedPeriod", null));
			List<Enterprise> enterpriseList = (List<Enterprise>) bu.listAll(conn, enterprise);
			if (enterpriseList.size()>0){
				enterprise = enterpriseList.get(0);
			}

			Period period = new Period();
			period.setId(periodId);
			bu.search(conn, period);

			// ResultSet book = mysql.callSingleSP(conn, "pListBook", null);

			List<EmployeeListDTO> employeeListDTO = getEmployeeListDTO(conn, period.getId(), enterprise.getId());

			request.setAttribute("Period", period);
			request.setAttribute("EnterpriseList", enterpriseList);
			request.setAttribute("EmployeeList", employeeListDTO);

			// request.setAttribute("Book", book);
			// request.setAttribute("Conn", conn);
		} catch (Exception e) {
			LOG.fatal(e);
		} finally {
			cf.closeConnection(conn);
		}
		forward(request, response, "/WEB-INF/jsp/remuneration/process/salary/process-salary-main.jsp");
		// request.getRequestDispatcher("/WEB-INF/jsp/remuneration/process/salary/process-salary.jsp").forward(request,
		// response);

	}

	private List<EmployeeListDTO> getEmployeeListDTO(Connection conn, Long periodId, Long enterpriseId) {
		String sql = "";
		sql += "SELECT a.cId AS cId, ";
		sql += "a.cRut as cRut, ";
		sql += "a.cName AS cName, ";
		sql += "b.cSalaryBase AS cSalaryBase, ";
		sql += "IfNull(c.cOvertimeTime, 0) AS cOvertimeTime, ";
		sql += "IfNull(c.cLiquidSalary, 0) AS cLiquidSalary ";
		sql += "FROM tEmployee AS a ";
		sql += "LEFT JOIN tAgreementSettlement  AS b ON a.cId = b.cEmployee ";
		sql += "LEFT JOIN tSettlement AS c ON b.cId = c.cAgreementSettlement ";
		sql += "LEFT JOIN tCostCenter AS d ON a.cCostCenter = d.cId ";
		sql += "LEFT JOIN tArea       AS e ON d.cArea = e.cId ";
		sql += "LEFT JOIN tEnterprise AS f ON e.cEnterprise = f.cid ";
		sql += "WHERE c.cPeriod=? OR c.cPeriod IS null ";
		sql += "AND f.cId=?;";

		BSmySQL mysql = new BSmySQL();
		List<EmployeeListDTO> out = new ArrayList<EmployeeListDTO>();
		ResultSet rs = mysql.queryResultSet(conn, sql, BSUtils.array2List(periodId, enterpriseId));

		try {
			while (rs.next()) {
				EmployeeListDTO theDTO = new EmployeeListDTO();
				theDTO.setId(rs.getLong("cId"));
				theDTO.setRut(rs.getString("cRut"));
				theDTO.setName(rs.getString("cName"));
				theDTO.setOvertimeTime(rs.getInt("cOvertimeTime"));
				theDTO.setBaseSalary(rs.getBigDecimal("cSalaryBase"));
				theDTO.setLiquidSalary(rs.getBigDecimal("cLiquidSalary"));
				out.add(theDTO);
			}
		} catch (SQLException e) {
			throw new BSDataBaseException(e);
		} finally {
			mysql.closeSQL(rs);
		}

		return out;
	}
}
