package cl.buildersoft.albizia.web.servlet.process;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.business.beans.HoursWorked;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

/**
 * Servlet implementation class SaveHoursWorked
 */
@WebServlet("/servlet/process/employee/SaveHoursWorked")
public class SaveHoursWorked extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(SaveHoursWorked.class);
	private static final long serialVersionUID = -6613423114094463351L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Long periodId = Long.parseLong(request.getParameter("PeriodId"));
		String[] employeeIds = request.getParameterValues("EmployeeId");
		BigDecimal hours = new BigDecimal(request.getParameter("Hours"));

		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);
		BSBeanUtils bu = new BSBeanUtils();
		HoursWorked hw = null;
		try {
			cf.setAutoCommit(conn, false);

			for (String employeeId : employeeIds) {
				hw = new HoursWorked();

				if (bu.search(conn, hw, "cPeriod=? AND cEmployee=?", periodId, employeeId)) {
					hw.setAmount(hours);
					bu.update(conn, hw);
				} else {
					hw.setEmployee(Long.parseLong(employeeId));
					hw.setPeriod(periodId);
					hw.setAmount(hours);
					bu.insert(conn, hw);
				}
			}
			cf.commit(conn);
		} catch (Exception e) {
			LOG.fatal(e);
			cf.rollback(conn);
		} finally {
			cf.closeConnection(conn);
		}
		String url = readURL(conn);

		forward(request, response, url);
	}

	private String readURL(Connection conn) {// HOURS_WORKED
		LOG.warn("Method not implemented");
		return "/servlet/process/employee/ReadHoursWorked";
	}
}
