package cl.buildersoft.albizia.web.servlet.process;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.business.dto.EmployeeHoursWorkedDTO;
import cl.buildersoft.albizia.service.PeriodService;
import cl.buildersoft.albizia.service.impl.PeriodServiceImpl;
import cl.buildersoft.dalea.bean.Period;
import cl.buildersoft.framework.database.BSmySQL;
import cl.buildersoft.framework.exception.BSDataBaseException;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/process/employee/ReadHoursWorked")
public class ReadHoursWorked extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(ReadHoursWorked.class);
	private static final long serialVersionUID = 8295288030298504113L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);
		try {

			PeriodService ps = new PeriodServiceImpl();
			Period period = ps.getOpenedPeriod(conn);
			String periodString = ps.periodAsString(period);
			List<EmployeeHoursWorkedDTO> ehw = listEmployee(conn, period);
			request.setAttribute("Period", periodString);
			request.setAttribute("PeriodId", period.getId());
			request.setAttribute("Employees", ehw);

		} catch (Exception e) {
			LOG.fatal(e);
		} finally {
			cf.closeConnection(conn);
		}

		forward(request, response, "/WEB-INF/jsp/employee/hours-worked-main.jsp");
	}

	private List<EmployeeHoursWorkedDTO> listEmployee(Connection conn, Period period) {
		String ps = "pListEmployeeHoursWorked";
		BSmySQL mysql = new BSmySQL();

		ResultSet rs = mysql.callSingleSP(conn, ps, period.getId());
		List<EmployeeHoursWorkedDTO> out = new ArrayList<EmployeeHoursWorkedDTO>();

		try {
			EmployeeHoursWorkedDTO dto = null;
			while (rs.next()) {
				dto = new EmployeeHoursWorkedDTO();
				dto.setId(rs.getLong("cId"));
				dto.setEmployeeId(rs.getLong("cEmployeeId"));
				dto.setName(rs.getString("cName"));
				dto.setRut(rs.getString("cRut"));
				dto.setAmount(rs.getBigDecimal("cAmount"));

				out.add(dto);
			}
		} catch (SQLException e) {
			LOG.fatal(e);
			throw new BSDataBaseException(e);
		}
		return out;
	}
}
