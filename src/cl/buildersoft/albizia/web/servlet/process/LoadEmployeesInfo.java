package cl.buildersoft.albizia.web.servlet.process;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.business.dto.EmployeeHoursWorkedDTO;
import cl.buildersoft.albizia.service.PeriodService;
import cl.buildersoft.albizia.service.impl.PeriodServiceImpl;
import cl.buildersoft.dalea.bean.Period;
import cl.buildersoft.framework.database.BSmySQL;
import cl.buildersoft.framework.exception.BSDataBaseException;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.util.BSUtils;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/process/employee/LoadEmployeesInfo")
public class LoadEmployeesInfo extends BSHttpServlet {
	private static final long serialVersionUID = 2337591828219130342L;

	private static final Logger LOG = LogManager.getLogger(LoadEmployeesInfo.class);

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Long periodId = Long.parseLong(request.getParameter("PeriodId"));
		String[] employeesId = request.getParameterValues("cId");
		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);
		BSmySQL mysql = new BSmySQL();
		List<EmployeeHoursWorkedDTO> employee = new ArrayList<EmployeeHoursWorkedDTO>();

		PeriodService ps = new PeriodServiceImpl();
		Period period = ps.getPeriod(conn, periodId);
		String periodString = ps.periodAsString(period);
		for (String id : employeesId) {
			ResultSet rs = mysql.callSingleSP(conn, "pGetEmployeeHoursWorked", BSUtils.array2List(periodId, id));
			EmployeeHoursWorkedDTO ehw = null;
			try {
				while (rs.next()) {
					ehw = new EmployeeHoursWorkedDTO(rs.getLong("cId"), rs.getLong("cEmployeeId"), rs.getString("cRut"),
							rs.getString("cName"), rs.getBigDecimal("cAmount"));
					employee.add(ehw);
				}
			} catch (SQLException e) {
				LOG.fatal(e);
				throw new BSDataBaseException(e);
			}
		}
		cf.closeConnection(conn);

		request.setAttribute("Period", periodString);
		request.setAttribute("PeriodId", periodId);
		request.setAttribute("Employee", employee);

		forward(request, response, "/WEB-INF/jsp/employee/hours-worked-commit-main.jsp");
	}

}
