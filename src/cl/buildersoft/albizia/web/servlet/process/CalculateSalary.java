package cl.buildersoft.albizia.web.servlet.process;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.service.SettlementService;
import cl.buildersoft.albizia.service.impl.SettlementServiceImpl;
import cl.buildersoft.dalea.business.dto.SettlementDTO;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/remuneration/process/salary/CalculateSalary")
public class CalculateSalary extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(CalculateSalary.class);
	private static final long serialVersionUID = 6118990651043646111L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String[] employeeIds = request.getParameterValues("cId");

		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);
		SettlementService ss = new SettlementServiceImpl();

		try {

			switch (employeeIds.length) {
			case 0:
				LOG.info("Do nothing, nothing selected");
				break;
			case 1:
				Long employeeId = Long.parseLong(employeeIds[0]);

				SettlementDTO settlement = ss.calculateOne(conn, employeeId);
				ss.saveSettlement(conn, settlement);
				break;
			default:
				LOG.info("More than one employee selected");
				break;
			}
		} catch (Exception e) {
			LOG.fatal(e);
		} finally {
			cf.closeConnection(conn);
		}

		/**
		 * BSmySQL mysql = new BSmySQL(); BSConnectionFactory cf = new
		 * BSConnectionFactory();
		 * 
		 * Connection conn = cf.getConnection(request);
		 * 
		 * mysql.callSingleSP(conn, "pCalculateAllSalary", null);
		 * 
		 * mysql.closeSQL(); cf.closeConnection(conn);
		 */

		forward(request, response, "/servlet/remuneration/process/salary/ProcessSalary");

		// request.getRequestDispatcher("/servlet/Home").forward(request,
		// response);
	}
}
