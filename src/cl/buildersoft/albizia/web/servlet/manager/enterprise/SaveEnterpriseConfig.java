package cl.buildersoft.albizia.web.servlet.manager.enterprise;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.buildersoft.dalea.bean.EnterpriseConfig;
import cl.buildersoft.framework.database.BSBeanUtilsSP;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/albizia/enterprise/SaveEnterpriseConfig")
public class SaveEnterpriseConfig extends BSHttpServlet {
	private static final long serialVersionUID = 5316369008384063620L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long enterpriseId = Long.parseLong(request.getParameter("cId"));

		BSBeanUtilsSP bu = new BSBeanUtilsSP();

		EnterpriseConfig enterpriseConfig = new EnterpriseConfig();
		enterpriseConfig.setId(enterpriseId);
		enterpriseConfig.setEnterprise(Long.valueOf(request.getParameter("enterprise")));
		enterpriseConfig.setShowDateUfUtm(validateCheck(request.getParameter("showDateUfUtm")));
		enterpriseConfig.setShowProfile(validateCheck(request.getParameter("showProfile")));
		enterpriseConfig.setShowCostCenter(validateCheck(request.getParameter("showCostCenter")));
		enterpriseConfig.setShowDataAgreement(validateCheck(request.getParameter("showDataAgreement")));
		enterpriseConfig.setShowSalaryRoot(validateCheck(request.getParameter("showSalary")));
		enterpriseConfig.setShowEmployerBonus(validateCheck(request.getParameter("showEmployerBonus")));
		enterpriseConfig.setShowWorkDay(validateCheck(request.getParameter("showWorkDay")));
		enterpriseConfig.setShowNetPaymentScope(validateCheck(request.getParameter("showNetPaymentScope")));
		enterpriseConfig.setTextFootSalary(request.getParameter("textFootSalary"));
		enterpriseConfig.setMailNotice(request.getParameter("email"));
		enterpriseConfig.setViewLastSettlements(Integer.parseInt(request.getParameter("cViewLastSettlements")));
		enterpriseConfig.setGratificationType(Long.parseLong(request.getParameter("GratificationType")));

		BSConnectionFactory cf = new BSConnectionFactory();

		Connection conn = cf.getConnection(request);
		try {
			bu.update(conn, enterpriseConfig);
		} finally {
			cf.closeConnection(conn);
		}

		forward(request, response, "/servlet/albizia/manager/EnterpriseManager");
	}

	private boolean validateCheck(String check) {
		Boolean out = false;
		if (check != null) {
			out = check.equalsIgnoreCase("on");
		}
		return out;
	}
	/**
	 * <code>
	private Account2 getAccountByEmployee(Connection conn, BSBeanUtilsSP bu,
			Long idEmployee) {
		return null;
	}
	</code>
	 */
}
