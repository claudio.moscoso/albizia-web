package cl.buildersoft.albizia.web.servlet.manager.enterprise;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.buildersoft.dalea.bean.EnterpriseConfig;
import cl.buildersoft.dalea.bean.GratificationType;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

/**
 * Servlet implementation class EditRecord
 */
@WebServlet("/servlet/albizia/enterprise/EnterpriseConfigServlet")
public class EnterpriseConfigServlet extends BSHttpServlet {

	private static final long serialVersionUID = -5785656616097922095L;

	public EnterpriseConfigServlet() {
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long enterpriseId = Long.parseLong(request.getParameter("cId"));

		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);
		EnterpriseConfig enterpriseConfig = null;
		List<GratificationType> gratificationTypeList = listGratificationList(conn);
		try {
			BSBeanUtils bu = new BSBeanUtils();

			enterpriseConfig = new EnterpriseConfig();
			enterpriseConfig.setId(enterpriseId);
			bu.search(conn, enterpriseConfig);
		} finally {
			cf.closeConnection(conn);
		}
		request.setAttribute("GratificationTypeList", gratificationTypeList);
		request.setAttribute("EnterpriseConfig", enterpriseConfig);

		forward(request, response, "/WEB-INF/jsp/enterprise/enterprise-config-main.jsp");
	}

	@SuppressWarnings("unchecked")
	private List<GratificationType> listGratificationList(Connection conn) {
		BSBeanUtils bu = new BSBeanUtils();
		return (List<GratificationType>) bu.listAll(conn, new GratificationType());
	}

}
