package cl.buildersoft.albizia.web.servlet.manager.period;

import java.sql.Connection;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import cl.buildersoft.framework.type.Semaphore;
import cl.buildersoft.framework.util.crud.BSAction;
import cl.buildersoft.framework.util.crud.BSActionType;
import cl.buildersoft.framework.util.crud.BSHttpServletCRUD;
import cl.buildersoft.framework.util.crud.BSTableConfig;

@WebServlet("/servlet/manager/period/PeriodManager")
public class PeriodManager extends BSHttpServletCRUD {
	private static final long serialVersionUID = -5347446304866453415L;

	@Override
	protected BSTableConfig getBSTableConfig(HttpServletRequest request) {
		BSTableConfig table = super.initTable(request, "tPeriod");
		table.setTitle("Períodos");

		table.setSortField("cDate");

		table.removeAction("INSERT");
		table.removeAction("DELETE");

		table.getField("cDate").setReadonly(true);

		// BSAction edit = table.getAction("EDIT");
		// edit.setUrl("/servlet/manager/period/UpdatePeriod");
		// edit.setContext("ALBIZIA_CONTEXT");

		table.addAction(newAction("DUPLICATE", "Duplicar", "/servlet/manager/period/ReadPeriod"));
		table.addAction(newAction("OPEN", "Abrir", "/servlet/manager/period/OpenPeriod"));

		// table.addAction(newAction("CLOSE", "Cerrar",
		// "/servlet/admin/period/ClosePeriod"));

		table.getField("cDate").setLabel("Fecha período");
		table.getField("cPeriodStatus").setLabel("Estado");
		table.getField("cPeriodStatus").setReadonly(true);
		table.getField("cUF").setLabel("U.F.");
		// table.getField("cOvertimeFactor").setLabel("Factor de horas extras");
		table.getField("cMinSalary").setLabel("Sueldo mínimo");
		table.getField("cLimitGratificationMonthly").setLabel("Tope Gratificación Mensual");
		table.getField("cGratificationAmount").setLabel("Gratificacion fija");
		table.getField("cEnterpriseLiquidIncome").setLabel("Entradas liquidas de la empresa");
		table.getField("cBonusPay").setLabel("Pago de bono");
		table.getField("cGratificationFactorEnterprise").setLabel("Factor de gratificacion");
		table.getField("cTotalRemunerations").setLabel("Total de remuneraciones");
		table.getField("cUnemploymentLimit").setLabel("Limite en UF para seguro de desempleo");
		table.getField("cAccidentInsuranceFactor").setLabel("Factor de seguro de accidente");

		table.getField("cUniqueTaxLimitAmount").setLabel("Limite de impuesto unico");
		// table.getField("cUniqueTaxAmount").setLabel("Monto de impuesto");
		table.getField("cUniqueTaxLimitCurrency").setLabel("Limite Moneda de impuesto");
		// table.getField("cUniqueTaxCurrency").setLabel("oneda de impuesto
		// unico");

		this.hideFields(table, "cMinSalary", "cLimitGratificationMonthly", "cGratificationAmount",
				"cEnterpriseLiquidIncome", "cBonusPay", "cGratificationFactorEnterprise", "cUnemploymentLimit",
				"cAccidentInsuranceFactor", "cUniqueTaxLimitAmount", "cUniqueTaxLimitCurrency");

		return table;
	}

	private BSAction newAction(String key, String label, String url) {
		BSAction out = new BSAction(key, BSActionType.Record);
		out.setUrl(url);
		out.setLabel(label);
		// out.setDisabled(true);
		out.setContext("ALBIZIA_CONTEXT");
		return out;
	}

	@Override
	public Semaphore setSemaphore(Connection conn, Object[] values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void configEventLog(BSTableConfig table, Long userId) {
		// TODO Auto-generated method stub

	}

}
