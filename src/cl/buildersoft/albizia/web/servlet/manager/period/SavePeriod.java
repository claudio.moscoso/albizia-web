package cl.buildersoft.albizia.web.servlet.manager.period;

import static cl.buildersoft.framework.util.BSDateTimeUtil.calendar2Date;

import java.io.IOException;
import java.sql.Connection;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.service.PeriodService;
import cl.buildersoft.albizia.service.impl.PeriodServiceImpl;
import cl.buildersoft.dalea.bean.Period;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.exception.BSUserException;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/manager/period/SavePeriod")
public class SavePeriod extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(SavePeriod.class);
	private static final long serialVersionUID = 1776719588359492973L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idString = request.getParameter("cId");
		Long id = null;
		Period period = new Period();
		PeriodService ps = new PeriodServiceImpl();

		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = null;
		try {
			conn = cf.getConnection(request);
			BSBeanUtils bu = new BSBeanUtils();
			Calendar calendar = Calendar.getInstance();
			{
				Integer year = Integer.parseInt(request.getParameter("cYear"));
				Integer month = Integer.parseInt(request.getParameter("cMonth"));
				calendar.set(Calendar.YEAR, year);
				calendar.set(Calendar.MONTH, month);
				calendar.set(Calendar.DAY_OF_MONTH, 1);
			}
			Boolean exists = dateExists(conn, bu, calendar);

			if (exists) {
				period.setDate(calendar2Date(calendar));
				throw new BSUserException("El paríodo seleccionado " + ps.periodAsString(period) + " ya existe");
			}

			// } else {
			id = (Long) Long.parseLong(idString);
			period.setId(id);
			bu.search(conn, period);
			// }

			period.setDate(calendar2Date(calendar));
			period.setPeriodStatus(1L);
			period.setId(null);

			// period.setDaysForYear(daysForYear);
			// period.setGratificationFactor(gratificationFactor);
			// period.setLimitGratification(limitGratification);
			// period.setLimitHealth(limitHealth);
			// period.setLimitInsurance(limitInsurance);
			// period.setLimitIPS(limitIPS);
			// period.setMinSalary(minSalary);
			// period.setOvertimeFactor(overtimeFactor);
			// period.setUf(uf);
			// period.setUtm(utm);

			bu.save(conn, period);
		} catch (Exception e) {
			LOG.fatal(e);
		} finally {
			cf.closeConnection(conn);
		}
		forward(request, response, "/servlet/manager/period/PeriodManager");
		// request.getRequestDispatcher("/servlet/admin/period/PeriodManager").forward(request,
		// response);
	}

	private Boolean dateExists(Connection conn, BSBeanUtils bu, Calendar calendar) {
		Integer size = bu.list(conn, new Period(), "DATE(cDate)=DATE(?)", calendar2Date(calendar)).size();
		return size > 0;
	}
}
