package cl.buildersoft.albizia.web.servlet.manager.period;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.service.PeriodService;
import cl.buildersoft.albizia.service.impl.PeriodServiceImpl;
import cl.buildersoft.dalea.bean.Period;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/manager/period/ReadPeriod")
public class ReadPeriod extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(ReadPeriod.class);
	private static final long serialVersionUID = -5718323555309174269L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("cId"));

		BSConnectionFactory cf = new BSConnectionFactory();

		Connection conn = cf.getConnection(request);

		PeriodService ps = new PeriodServiceImpl();
		try {
			Period period = ps.getPeriod(conn, id);

			request.setAttribute("Period", period);
			request.setAttribute("PeriodName", ps.periodAsString(period));
			period.setPeriodStatus(1L);
			request.setAttribute("StatusName", ps.getStatusName(conn, period));
		} catch (Exception e) {
			LOG.fatal(e);
		} finally {
			cf.closeConnection(conn);
		}
		forward(request, response, "/WEB-INF/jsp/period/period-duplicate-main.jsp");

		// request.getRequestDispatcher("/WEB-INF/jsp/period/period-duplicate-main.jsp").forward(request,
		// response);
	}

}
