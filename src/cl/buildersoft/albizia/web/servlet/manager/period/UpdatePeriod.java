package cl.buildersoft.albizia.web.servlet.manager.period;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.service.PeriodService;
import cl.buildersoft.albizia.service.impl.PeriodServiceImpl;
import cl.buildersoft.dalea.bean.Period;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/manager/period/UpdatePeriod")
public class UpdatePeriod extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(UpdatePeriod.class);
	private static final long serialVersionUID = -1153188022702560226L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("cId"));

		String url = "/WEB-INF/jsp/remuneration/period/period-main.jsp";

		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);

		PeriodService ps = new PeriodServiceImpl();
		try {
			Period period = ps.getPeriod(conn, id);

			request.setAttribute("Period", period);
			request.setAttribute("PeriodName", ps.periodAsString(period));
			request.setAttribute("StatusName", ps.getStatusName(conn, period));

			if (!period.getPeriodStatus().equals(2L)) {
				url = "/WEB-INF/jsp/remuneration/period/period-cant-change.jsp";
			}
		} catch (Exception e) {
			LOG.fatal(e);
		} finally {
			cf.closeConnection(conn);
		}
		forward(request, response, url);
		// request.getRequestDispatcher(url).forward(request, response);
	}

}
