package cl.buildersoft.albizia.web.servlet.manager.period;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.service.PeriodService;
import cl.buildersoft.albizia.service.impl.PeriodServiceImpl;
import cl.buildersoft.dalea.bean.Period;
import cl.buildersoft.framework.database.BSmySQL;
import cl.buildersoft.framework.exception.BSProgrammerException;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

public class AbstractPeriodServlet extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(AbstractPeriodServlet.class);
	private static final long serialVersionUID = 5175580005574523069L;

	protected String changeStatus(HttpServletRequest request, Status oldStatus, Status newStatus) {
		Long idPeriod = Long.parseLong(request.getParameter("cId"));

		BSConnectionFactory cf = new BSConnectionFactory();
		String url = "/servlet/manager/period/PeriodManager";
		Connection conn = null;
		try {
			conn = cf.getConnection(request);

			PeriodService ps = new PeriodServiceImpl();
			Period period = ps.getPeriod(conn, idPeriod);

			Status status = getStatus(period);
			if (status.equals(oldStatus)) {
				OpenOrClosePeriod(conn, period, newStatus);
			} else {
				setPeriodInfoToRequest(request, conn, ps, period);

				url = "/WEB-INF/jsp/period/period-cant-change-main.jsp";
			}
		} catch (Exception e) {
			LOG.fatal(e);
		} finally {
			cf.closeConnection(conn);
		}
		return url;
	}

	protected void OpenOrClosePeriod(Connection conn, Period period, Status status) {
		BSmySQL mysql = new BSmySQL();
		if (status.equals(Status.INIT)) {
			throw new BSProgrammerException("You can change status to Open or Close");
		}

		String spName = status.equals(Status.OPEN) ? "pOpenPeriod" : "pClosePeriod";
		mysql.callSingleSP(conn, spName, period.getId());
	}

	protected Status getStatus(Period period) {
		Status out = null;

		switch (period.getPeriodStatus().intValue()) {
		case 1:
			out = Status.INIT;
			break;
		case 2:
			out = Status.OPEN;
			break;
		case 3:
			out = Status.CLOSE;
			break;
		}

		return out;
	}

	protected void setPeriodInfoToRequest(HttpServletRequest request, Connection conn, PeriodService ps,
			Period period) {
		request.setAttribute("PeriodName", ps.periodAsString(period));
		request.setAttribute("StatusName", ps.getStatusName(conn, period));
	}
}

enum Status {
	INIT, OPEN, CLOSE;
}