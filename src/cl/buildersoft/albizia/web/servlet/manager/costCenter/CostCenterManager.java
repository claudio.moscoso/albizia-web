package cl.buildersoft.albizia.web.servlet.manager.costCenter;

import java.sql.Connection;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import cl.buildersoft.framework.type.Semaphore;
import cl.buildersoft.framework.util.crud.BSHttpServletCRUD;
import cl.buildersoft.framework.util.crud.BSTableConfig;

/**
 * Servlet implementation class CostCenterManager
 */
@WebServlet("/servlet/albizia/manager/CostCenterManager")
public class CostCenterManager extends BSHttpServletCRUD {

	private static final long serialVersionUID = -9151208219300016206L;

	@Override
	protected void configEventLog(BSTableConfig arg0, Long arg1) {
	}

	@Override
	protected BSTableConfig getBSTableConfig(HttpServletRequest request) {
		BSTableConfig table = super.initTable(request, "tCostCenter");
		
		table.setTitle("Centros de costo");
		table.getField("cName").setLabel("Nombre");
		table.getField("cArea").setLabel("Área");
		table.setFilterFields("cArea");
		
		return table;
	}

	@Override
	public Semaphore setSemaphore(Connection arg0, Object[] arg1) {
		return null;
	}

}
