package cl.buildersoft.albizia.web.servlet.manager.employeeAgreement;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.buildersoft.albizia.business.beans.RagreementAPV;
import cl.buildersoft.dalea.bean.Agreement;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.database.BSmySQL;
import cl.buildersoft.framework.exception.BSDataBaseException;
import cl.buildersoft.framework.type.Currency;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.util.BSUtils;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/config/employee/SavePrevitionalInfo")
public class SavePrevitionalInfo extends BSHttpServlet {
	private static final long serialVersionUID = 5316369008384063620L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long employeeId = Long.parseLong(request.getParameter("cId"));

		BSConnectionFactory cf = new BSConnectionFactory();
		BSmySQL mysql = new BSmySQL();
		Connection conn = cf.getConnection(request);
		BSBeanUtils bu = new BSBeanUtils();

		// Board apvType = (Board)bu.get(conn, new Board(),
		// "pGetBoardByTypeAndKey", array2List("INSTITUTION", "APV"));

		cf.setAutoCommit(conn, false);
		try {
			Agreement agreement = getAgreement(conn, employeeId);
			mysql.callSingleSP(conn, "pDelAgreementAPVByEmployee", employeeId);

			String[] apvInstitution = (String[]) request.getParameterValues("apvInstitution");
			String[] apvCurrency = (String[]) request.getParameterValues("apvCurrency");
			String[] apvAmount = (String[]) request.getParameterValues("apvAmount");

			for (int i = 0; apvInstitution != null && i < apvInstitution.length; i++) {
				RagreementAPV agreementAPV = new RagreementAPV();

				agreementAPV.setAgreement(agreement.getId());
				agreementAPV.setAmount(new Double(apvAmount[i]));
				agreementAPV.setApv(new Long(apvInstitution[i]));
				agreementAPV.setCurrency(new Long(apvCurrency[i]));

				mysql.callSingleSP(conn, "pSaveAPVForAgreement", BSUtils.array2List(agreementAPV.getAgreement(),
						agreementAPV.getApv(), agreementAPV.getCurrency(), agreementAPV.getAmount()));

			}

			Long exBox = Long.valueOf(request.getParameter("exBox"));
			Integer disabilityBurdens = Integer.valueOf(request.getParameter("DisabilityBurdens"));
			Integer maternalLoad = Integer.valueOf(request.getParameter("MaternalLoad"));
			Long afpEmp = Long.valueOf(request.getParameter("afpEmp"));
			Integer monthsQuoted = Integer.parseInt(request.getParameter("MonthsQuoted"));
			String healthCurrency = request.getParameter("HealthCurrency");

			Long health = Long.valueOf(request.getParameter("health"));
			Double healthAmount = Double.valueOf(request.getParameter("HealthAmount"));
			Integer simpleLoads = Integer.valueOf(request.getParameter("SimpleLoad"));
			Boolean pensionary = Boolean.parseBoolean(request.getParameter("Pensionary"));
			Long familyAssignmentStretch = Long.parseLong(request.getParameter("FamilyAssignmentStretch"));

			String currencyAccount2 = request.getParameter("CurrencyAccount2");
			Double amountAccount2 = Double.parseDouble(request.getParameter("AmountAccount2"));

			String additionalPFMCurrency = request.getParameter("AdditionalPFMCurrency");
			Double additionalPFMAmount = Double.parseDouble(request.getParameter("AdditionalPFMAmount"));

			agreement.setExBoxSystem(exBox);
			agreement.setDisabilityBurdens(disabilityBurdens);
			agreement.setMaternalLoads(maternalLoad);
			agreement.setPfm(afpEmp);
			agreement.setMonthsQuoted(monthsQuoted);
			
			agreement.setHealthCurrency(Currency.valueOf(healthCurrency));
			agreement.setHealth(health);
			agreement.setHealthAmount(BigDecimal.valueOf(healthAmount));
			agreement.setSimpleLoads(simpleLoads);
			agreement.setPensionary(pensionary);
			agreement.setFamilyAssignmentStretch(familyAssignmentStretch);
			agreement.setAccount2Currency(Currency.valueOf(currencyAccount2));
			agreement.setAccount2Amount(BigDecimal.valueOf(amountAccount2));
			agreement.setAdditionalPFMAmount(BigDecimal.valueOf(additionalPFMAmount));
			agreement.setAdditionalPFMCurrency(Currency.valueOf(additionalPFMCurrency));

			bu.save(conn, agreement);
			cf.commit(conn);
		} catch (BSDataBaseException e) {
			cf.rollback(conn);
		} finally {
			cf.closeConnection(conn);
		}

		forward(request, response, "/servlet/config/employee/EmployeeAgreementManager");
	}

	private Agreement getAgreement(Connection conn, Long idEmployee) {
		ContractualInfo ci = new ContractualInfo();
		return ci.getAgreement(conn, idEmployee);
	}
	/**
	 * <code>
	private Account2 getAccountByEmployee(Connection conn, BSBeanUtilsSP bu,
			Long idEmployee) {
		return null;
	}
	</code>
	 */
}
