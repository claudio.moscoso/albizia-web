package cl.buildersoft.albizia.web.servlet.manager.employeeAgreement;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.business.beans.APV;
import cl.buildersoft.albizia.business.beans.ExBoxSystem;
import cl.buildersoft.albizia.business.beans.FamilyAssignmentStretch;
import cl.buildersoft.albizia.business.beans.RagreementAPV;
import cl.buildersoft.dalea.bean.Agreement;
import cl.buildersoft.dalea.bean.Employee;
import cl.buildersoft.dalea.bean.Health;
import cl.buildersoft.dalea.bean.PFM;
import cl.buildersoft.dalea.services.AgreementService;
import cl.buildersoft.dalea.services.EmployeeService;
import cl.buildersoft.dalea.services.impl.AgreementServiceImpl;
import cl.buildersoft.dalea.services.impl.EmployeeServiceImpl;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.database.BSmySQL;
import cl.buildersoft.framework.exception.BSDataBaseException;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/config/employee/InformationPrevitional")
public class InformationPrevitional extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(InformationPrevitional.class);
	private static final long serialVersionUID = -5785656616097922095L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Long employeeId = Long.parseLong(request.getParameter("cId"));

		BSConnectionFactory cf = new BSConnectionFactory();
		BSmySQL mysql = new BSmySQL();
		Connection conn = cf.getConnection(request);
		BSBeanUtils bu = new BSBeanUtils();

		EmployeeService service = new EmployeeServiceImpl();

		List<APV> listadoApv = null;
		List<PFM> listadoAfp = null;
		List<RagreementAPV> listadoApvEmp = null;
		List<ExBoxSystem> listadoExBox = null;
		List<Health> listadoHealth = null;
		Agreement agreementEmp = null;
		Employee employee = null;
		List<FamilyAssignmentStretch> familyAssignmentStretch = null;
		try {
			employee = service.getEmployee(conn, employeeId);

			listadoApv = (List<APV>) bu.listAll(conn, new APV());
			listadoAfp = (List<PFM>) bu.listAll(conn, new PFM());
			// List<Currency> listadoCurrency = (List<Currency>)
			// bu.listAll(conn, new Currency());
			listadoHealth = (List<Health>) bu.listAll(conn, new Health());
			listadoExBox = (List<ExBoxSystem>) bu.listAll(conn, new ExBoxSystem());
			familyAssignmentStretch = (List<FamilyAssignmentStretch>) bu.listAll(conn, new FamilyAssignmentStretch());
			listadoApvEmp = listAPVForEmployee(conn, mysql, employeeId);
			agreementEmp = getAgreement(conn, bu, employeeId);
		} catch (Exception e) {
			LOG.fatal(e);
		} finally {
			cf.closeConnection(conn);
		}
		request.setAttribute("listadoAfp", listadoAfp);
		// request.setAttribute("listadoApv", listadoApv);
		// request.setAttribute("listadoCurrency", listadoCurrency);
		// request.setAttribute("listadoApvEmp", listadoApvEmp);
		request.setAttribute("listadoExBox", listadoExBox);
		request.setAttribute("listadoHealth", listadoHealth);
		request.setAttribute("agreementEmp", agreementEmp);
		request.setAttribute("FamilyAssignmentStretch", familyAssignmentStretch);
		request.setAttribute("Employee", employee);

		request.setAttribute("Action", "Update");
		forward(request, response, "/WEB-INF/jsp/employee/previtional-information-main.jsp");
		// request.getRequestDispatcher("/WEB-INF/jsp/config/employee/previtional-information.jsp").forward(request,
		// response);
	}

	private List<RagreementAPV> listAPVForEmployee(Connection conn, BSmySQL mysql, Long employeeId) {
		ResultSet rs = mysql.callSingleSP(conn, "pListAPVForEmployee", employeeId);

		// List<RagreementAPV> listadoApvEmp = (List<RagreementAPV>)
		// bu.list(conn,
		// new RagreementAPV(), "pListAPVForEmployee", parameter);
		List<RagreementAPV> out = new ArrayList<RagreementAPV>();
		try {
			while (rs.next()) {
				RagreementAPV agreementAPV = new RagreementAPV();
				agreementAPV.setAgreement(rs.getLong("cAgreement"));
				agreementAPV.setApv(rs.getLong("cAPV"));
				agreementAPV.setCurrency(rs.getLong("cCurrency"));
				agreementAPV.setAmount(rs.getDouble("cAmount"));
				out.add(agreementAPV);
			}
		} catch (SQLException e) {
			throw new BSDataBaseException(e);
		}

		return out;
	}

	public Agreement getAgreement(Connection conn, BSBeanUtils bu, Long idEmployee) {
		AgreementService agreementService = new AgreementServiceImpl();
		Agreement out = agreementService.getAgreementByEmployee(conn, idEmployee);
		return out;
	}

}
