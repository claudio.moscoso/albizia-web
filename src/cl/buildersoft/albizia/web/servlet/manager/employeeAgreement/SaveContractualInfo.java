package cl.buildersoft.albizia.web.servlet.manager.employeeAgreement;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.buildersoft.dalea.bean.Agreement;
import cl.buildersoft.dalea.bean.ContractType;
import cl.buildersoft.dalea.type.HiredType;
import cl.buildersoft.framework.dataType.BSDataType;
import cl.buildersoft.framework.dataType.BSDataTypeEnum;
import cl.buildersoft.framework.dataType.BSDataTypeFactory;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.database.BSBeanUtilsSP;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/config/employee/SaveContractualInfo")
public class SaveContractualInfo extends BSHttpServlet {
	private static final long serialVersionUID = 5316369008384063620L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("cId"));

		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);
		BSBeanUtilsSP bu = new BSBeanUtilsSP();

		BSDataTypeFactory dtf = new BSDataTypeFactory();
		BSDataType dateType = dtf.create(BSDataTypeEnum.DATE);
		BSDataType doubleType = dtf.create(BSDataTypeEnum.DOUBLE);

		Long profile = Long.parseLong(request.getParameter("cPosition"));
		Long contractTypeId = Long.parseLong(request.getParameter("cContractType"));

		try {
			Agreement agreement = getAgreement(conn, id);
			ContractType contractType = getContractType(conn, contractTypeId);

			Date startContract = (Date) dateType.parse(conn, request.getParameter("cStartContract"));
			Date endContract = contractType.getKey().equals("UND") ? null
					: (Date) dateType.parse(conn, request.getParameter("cEndContract"));
			// Long gratificationType =
			// Long.parseLong(request.getParameter("cGratificationType"));
			// Long horary = Long.parseLong(request.getParameter("cHorary"));

			Double mobilization = (Double) doubleType.parse(conn, request.getParameter("cMobilization"));
			Double feeding = (Double) doubleType.parse(conn, request.getParameter("cFeeding"));
			Double salaryRoot = (Double) doubleType.parse(conn, request.getParameter("cSalaryRoot"));
			Integer hiredAmount = Integer.parseInt(request.getParameter("cHiredAmount"));
			String hiredType = request.getParameter("cHiredType");

			agreement.setPosition(profile);
			agreement.setContractType(contractTypeId);
			agreement.setStartContract(startContract);
			agreement.setEndContract(endContract);
			// agreement.setGratificationType(gratificationType);
			// agreement.setHorary(horary);
			agreement.setMobilization(BigDecimal.valueOf(mobilization));
			agreement.setFeeding(BigDecimal.valueOf(feeding));
			agreement.setSalaryBase(BigDecimal.valueOf(salaryRoot));
			agreement.setHiredAmount(hiredAmount);
			agreement.setHiredType(HiredType.valueOf(hiredType));

			bu.save(conn, agreement);
		} finally {
			cf.closeConnection(conn);
		}

		forward(request, response, "/servlet/config/employee/EmployeeAgreementManager");
		// request.getRequestDispatcher("/servlet/config/employee/EmployeeManager").forward(request,
		// response);
	}

	private ContractType getContractType(Connection conn, Long contractTypeId) {
		BSBeanUtils bu = new BSBeanUtils();
		ContractType contractType = new ContractType();
		contractType.setId(contractTypeId);
		bu.search(conn, contractType);

		return contractType;
	}

	private Agreement getAgreement(Connection conn, Long idEmployee) {
		ContractualInfo ci = new ContractualInfo();
		return ci.getAgreement(conn, idEmployee);
	}

}
