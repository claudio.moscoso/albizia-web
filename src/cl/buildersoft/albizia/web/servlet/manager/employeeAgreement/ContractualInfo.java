package cl.buildersoft.albizia.web.servlet.manager.employeeAgreement;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.dalea.bean.Agreement;
import cl.buildersoft.dalea.bean.ContractType;
import cl.buildersoft.dalea.bean.Employee;
import cl.buildersoft.dalea.bean.GratificationType;
import cl.buildersoft.dalea.bean.Position;
import cl.buildersoft.dalea.services.AgreementService;
import cl.buildersoft.dalea.services.impl.AgreementServiceImpl;
import cl.buildersoft.framework.beans.BSBean;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.exception.BSException;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.util.BSDateTimeUtil;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/config/employee/ContractualInfo")
public class ContractualInfo extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(ContractualInfo.class);
	private static final long serialVersionUID = -8599267568451620681L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("cId"));
		// BSmySQL mysql = new BSmySQL();
		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);
		BSBeanUtils bu = new BSBeanUtils();

		Employee emp = null;
		Agreement agreement = null;

		List<BSBean> position = null;
		List<BSBean> contractTypes = null;
		List<BSBean> gratificationType = null;
		List<BSBean> horary = null;

		try {
			emp = getEmployee(conn, bu, id);
			agreement = getAgreement(conn, id);

			position = getListAll(conn, bu, new Position());
			contractTypes = getListAll(conn, bu, new ContractType());
			gratificationType = getListAll(conn, bu, new GratificationType());
			// horary = getListAll(conn, bu, new Horary());

			request.setAttribute("Horary", horary);
			request.setAttribute("GratificationType", gratificationType);
			request.setAttribute("DateFormat", BSDateTimeUtil.getFormatDate(conn));
			request.setAttribute("Employee", emp);
			request.setAttribute("Agreement", agreement);
			request.setAttribute("Position", position);
			request.setAttribute("ContractTypes", contractTypes);
			// cf.closeConnection(conn);
		} catch (BSException e) {
			LOG.fatal(e);
		} finally {
			cf.closeConnection(conn);
		}

		forward(request, response, "/WEB-INF/jsp/config/employee/contractual-info-main.jsp");
		// request.getRequestDispatcher("/WEB-INF/jsp/config/employee/contractual-info.jsp").forward(request,
		// response);
	}

	private List<BSBean> getListAll(Connection conn, BSBeanUtils bu, BSBean object) {
		List<BSBean> out = (List<BSBean>) bu.listAll(conn, object);
		return out;
	}

	public Agreement getAgreement(Connection conn, Long idEmployee) {
		AgreementService agreementService = new AgreementServiceImpl();
		Agreement out = agreementService.getAgreementByEmployee(conn, idEmployee);
		return out;
	}

	private Employee getEmployee(Connection conn, BSBeanUtils bu, Long id) {
		Employee out = new Employee();
		out.setId(id);
		bu.search(conn, out);
		return out;
	}
}
