package cl.buildersoft.albizia.web.servlet.manager.pfm;

import java.sql.Connection;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import cl.buildersoft.framework.type.Semaphore;
import cl.buildersoft.framework.util.crud.BSHttpServletCRUD;
import cl.buildersoft.framework.util.crud.BSTableConfig;

@WebServlet("/servlet/config/pfm/PFMValueManager")
public class PFMValueManager extends BSHttpServletCRUD {
	private static final long serialVersionUID = -913523455848692014L;

	@Override
	protected BSTableConfig getBSTableConfig(HttpServletRequest request) {
		BSTableConfig table = super.initTable(request, "tPFMValue");

		table.setTitle("Administradores de Fondos de Pensión - Configuración de valores");

//		table.getField("cKey").setLabel("Llave de integración");
//		table.getField("cName").setLabel("Nombre");
//		table.getField("cFactor").setLabel("Factor descuento");
//		table.getField("cSIS").setLabel("Seguro de Invalidez y Sobrevivencia");
//		hideFields(table, "cUpdated");
		
		table.setFilterFields("cPFM");

		return table;
	}

	@Override
	public Semaphore setSemaphore(Connection conn, Object[] values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void configEventLog(BSTableConfig table, Long userId) {
		// TODO Auto-generated method stub

	}

	
}