package cl.buildersoft.albizia.web.servlet.manager.pfm;

import java.sql.Connection;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import cl.buildersoft.framework.type.Semaphore;
import cl.buildersoft.framework.util.crud.BSHttpServletCRUD;
import cl.buildersoft.framework.util.crud.BSTableConfig;
@WebServlet("/servlet/config/pfm/PFMListManager")
public class PFMListManager extends BSHttpServletCRUD {
	private static final long serialVersionUID = 142764358133309149L;

	@Override
	protected BSTableConfig getBSTableConfig(HttpServletRequest request) {
		BSTableConfig table = super.initTable(request, "tPFM");

		table.setTitle("Administradores de Fondos de Pensión");
		
		return table;
	}

	@Override
	public Semaphore setSemaphore(Connection conn, Object[] values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void configEventLog(BSTableConfig table, Long userId) {
		// TODO Auto-generated method stub
	}
}
