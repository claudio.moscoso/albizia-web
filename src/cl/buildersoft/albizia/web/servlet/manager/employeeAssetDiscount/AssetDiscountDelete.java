package cl.buildersoft.albizia.web.servlet.manager.employeeAssetDiscount;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.business.beans.AgreementAsset;
import cl.buildersoft.albizia.business.beans.AgreementDiscount;
import cl.buildersoft.framework.beans.BSBean;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/manager/employee/AssetDiscountDelete")
public class AssetDiscountDelete extends BSHttpServlet {
	private static final long serialVersionUID = 8845931038915488289L;
	private static final Logger LOG = LogManager.getLogger(AssetDiscountDelete.class);

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		String employee = request.getParameter("cEmployee");
		String id = request.getParameter("cAssetDicountId");
		String type = request.getParameter("cType");

		BSBeanUtils bu = new BSBeanUtils();
		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);

		if (type.equalsIgnoreCase("Asset")) {
			AgreementAsset assetOrDicount = new AgreementAsset();
			assetOrDicount.setId(Long.parseLong(id));
			bu.delete(conn, assetOrDicount);
		} else {
			AgreementDiscount assetOrDicount = new AgreementDiscount();
			assetOrDicount.setId(Long.parseLong(id));
			bu.delete(conn, assetOrDicount);

		}

		cf.closeConnection(conn);

//		request.setAttribute("cId", employee);
		request.setAttribute("cType", type);
		

		forward(request, response, "/servlet/manager/employee/AssetDiscountServlet");
	}
}
