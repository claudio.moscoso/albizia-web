package cl.buildersoft.albizia.web.servlet.manager.employeeAssetDiscount;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.business.beans.AgreementAsset;
import cl.buildersoft.albizia.business.beans.AgreementDiscount;
import cl.buildersoft.dalea.type.AssetType;
import cl.buildersoft.dalea.type.DiscountType;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.database.BSmySQL;
import cl.buildersoft.framework.exception.BSProgrammerException;
import cl.buildersoft.framework.type.Currency;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/manager/employee/AssetDiscountSave")
public class AssetDiscountSave extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(AssetDiscountSave.class);
	private static final long serialVersionUID = 4541324920181198318L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		BSmySQL mysql = new BSmySQL();
		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);

		mysql.setAutoCommit(conn, Boolean.FALSE);
		String assetDiscount = request.getParameter("AssetDiscount");
		LOG.debug(assetDiscount);

		// Long period = Long.parseLong(request.getParameter("cPeriod"));
		Long employee = Long.parseLong(request.getParameter("cEmployee"));

		// Long book = null;

		try {
			/**
			 * Agreement 1 cEmployee 81 AssetDiscount ASSET AssetType ASSIGNMENT
			 * AssetName abc Currency CLP Amount 0
			 */
			Long agreement = Long.parseLong(request.getParameter("Agreement").toString());
			
			LOG.debug(request.getParameter("Amount").toString());
			
			BigDecimal amount = new BigDecimal(request.getParameter("Amount").toString());
			Currency currency = Currency.valueOf(request.getParameter("Currency").toString());

			BSBeanUtils bu = new BSBeanUtils();

			if ("ASSET".equalsIgnoreCase(assetDiscount)) {
				AgreementAsset agreementAsset = new AgreementAsset();
				agreementAsset.setAgreement(agreement);
				agreementAsset.setAmount(amount);
				agreementAsset.setAssetType(AssetType.valueOf(request.getParameter("AssetType").toString()));
				agreementAsset.setCurrency(currency);
				agreementAsset.setKey(request.getParameter("Key").toString());
				agreementAsset.setLabel(request.getParameter("Name").toString());
				bu.insert(conn, agreementAsset);
			} else {
				AgreementDiscount agreementDiscount = new AgreementDiscount();
				agreementDiscount.setAgreement(agreement);
				agreementDiscount.setAmount(amount);
				agreementDiscount.setCurrency(currency);
				agreementDiscount.setKey(request.getParameter("Key").toString());
				agreementDiscount
						.setDiscountType(DiscountType.valueOf(request.getParameter("DiscountType").toString()));
				agreementDiscount.setLabel(request.getParameter("Name").toString());
				bu.insert(conn, agreementDiscount);
			}

			// saveAsset(request, mysql, conn, period, employee);
			// saveAssetAndDiscount(request, mysql, conn, period, employee, 1L);
			//
			// saveDiscount(request, mysql, conn, period, employee);
			// saveAssetAndDiscount(request, mysql, conn, period, employee, 2L);

			mysql.commit(conn);
		} catch (Exception e) {
			mysql.rollback(conn);
			throw new BSProgrammerException(e);
		} finally {
			mysql.closeConnection(conn);
		}

		request.setAttribute("cId", employee);
		
		forward(request, response, "/servlet/manager/employee/AssetDiscountServlet");
//		forward(request, response, "/servlet/config/employee/EmployeeAgreementManager");
	}

	/**
	 * <code>
	private Long createBookIfNotExists(Connection conn, Long period, Long employee) {
		BSmySQL mysql = new BSmySQL();
		String book = mysql.callFunction(conn, "fSaveBookForEmployee", array2List(period, employee));
		return Long.parseLong(book);
	}
	</code>
	 */

	private void saveAssetAndDiscount(HttpServletRequest request, BSmySQL mysql, Connection conn, Long period,
			Long employee, Long type) {
		List<Object> params = new ArrayList<Object>();
		String fieldName, fieldValue = null;
		String preFix = type.equals(1L) ? "cB" : "cD";
		Double value = null;
		params.add(period);
		params.add(employee);
		params.add(type);
		for (int i = 1; i < 11; i++) {
			fieldName = preFix + (i < 10 ? "0" + i : i);
			fieldValue = request.getParameter(fieldName);

			fieldValue = "".equals(fieldValue) ? "0" : fieldValue;

			if (fieldValue != null) {
				value = Double.parseDouble(fieldValue);
				params.add(fieldName);
				params.add(value);

				mysql.callSingleSP(conn, "pSaveAssetOrDiscount", params);

				params.remove(4);
				params.remove(3);
			}
		}
	}

	private void saveAsset(HttpServletRequest request, BSmySQL mysql, Connection conn, Long period, Long employee) {
		Double participation = Double.parseDouble(request.getParameter("cParticipation"));
		Double extraPay = Double.parseDouble(request.getParameter("cExtraPay"));
		Double ias = Double.parseDouble(request.getParameter("cIAS"));
		Double bounty = Double.parseDouble(request.getParameter("cBounty"));
		Double familyRetroactive = Double.parseDouble(request.getParameter("cFamilyRetroactive"));
		Double monthNotification = Double.parseDouble(request.getParameter("cMonthNotification"));
		Double feeding = Double.parseDouble(request.getParameter("cFeeding"));
		Double mobilization = Double.parseDouble(request.getParameter("cMobilization"));

		List<Object> parameters = new ArrayList<Object>();
		parameters.add(period);
		parameters.add(employee);
		parameters.add(participation);
		parameters.add(extraPay);
		parameters.add(ias);
		parameters.add(bounty);
		parameters.add(familyRetroactive);
		parameters.add(monthNotification);
		parameters.add(feeding);
		parameters.add(mobilization);

		mysql.callSingleSP(conn, "pSaveAsset", parameters);
	}

	private void saveDiscount(HttpServletRequest request, BSmySQL mysql, Connection conn, Long period, Long employee) {
		Double loanEnterprise = Double.parseDouble(request.getParameter("cLoanEnterprise"));
		Double loanCompensationFund = Double.parseDouble(request.getParameter("cLoanCompensationFund"));
		Double savingCompensationFund = Double.parseDouble(request.getParameter("cSavingCompensationFund"));
		Double judicialRetention = Double.parseDouble(request.getParameter("cJudicialRetention"));

		List<Object> parameters = new ArrayList<Object>();
		parameters.add(period);
		parameters.add(employee);
		parameters.add(loanEnterprise);
		parameters.add(loanCompensationFund);
		parameters.add(savingCompensationFund);
		parameters.add(judicialRetention);

		mysql.callSingleSP(conn, "pSaveDiscount", parameters);
	}

}
