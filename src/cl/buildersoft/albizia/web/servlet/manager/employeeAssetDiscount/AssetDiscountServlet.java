package cl.buildersoft.albizia.web.servlet.manager.employeeAssetDiscount;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.business.beans.AgreementAsset;
import cl.buildersoft.albizia.business.beans.AgreementDiscount;
import cl.buildersoft.dalea.bean.Agreement;
import cl.buildersoft.dalea.bean.Employee;
import cl.buildersoft.dalea.services.AgreementService;
import cl.buildersoft.dalea.services.EmployeeService;
import cl.buildersoft.dalea.services.impl.AgreementServiceImpl;
import cl.buildersoft.dalea.services.impl.EmployeeServiceImpl;
import cl.buildersoft.framework.database.BSBeanUtils;
import cl.buildersoft.framework.exception.BSSystemException;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;

@WebServlet("/servlet/manager/employee/AssetDiscountServlet")
public class AssetDiscountServlet extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(AssetDiscountServlet.class);
	private static final long serialVersionUID = 6336621423830251342L;

	/**
	 * Este Servlet debe leer las tablas tAgreementAsset y tAgreementDiscount
	 * para mostrarlas en la pantalla. Esta informacion corresponde a los
	 * Haberes y Descuentos asociados a un contrato.
	 * 
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);

		try {
			BSBeanUtils bu = new BSBeanUtils();

			// Period period = getPeriod(conn);
			Employee employee = getEmployee(conn, request);
			Agreement agreement = getAgreement(conn, employee);
			List<AgreementAsset> assets = getAsset(conn, agreement);
			List<AgreementDiscount> discounts = getDiscount(conn, agreement);
			/********************/
			// List<AssetDiscount> assetDiscount = (List<AssetDiscount>)
			// bu.listAll(conn, new AssetDiscount());
			// List<Object> params = getParams(period, employee);
			// BSmySQL mysql = new BSmySQL();
			// ResultSet assetDiscountData = mysql.callSingleSP(conn,
			// "pGetAssetDiscount", params);

			// request.setAttribute("Period", period);
			request.setAttribute("Employee", employee);
			request.setAttribute("Agreement", agreement);
			request.setAttribute("Asset", assets);
			request.setAttribute("Discount", discounts);

			// request.setAttribute("AssetDiscount", assetDiscount);
			// request.setAttribute("AssetDiscountData", assetDiscountData);
			// request.setAttribute("Conn", conn);
			// request.setAttribute("PeriodName",
			// BSDateTimeUtil.month2Word(period.getDate()) + " de " +
			// BSDateTimeUtil.getYear(period.getDate()));
		} catch (Exception e) {
			LOG.fatal(e);
			throw new BSSystemException(e);
		} finally {
			cf.closeConnection(conn);
		}

		// redirect(request, response, "/dalea-web/servlet/ShowParameters");
		forward(request, response, "/WEB-INF/jsp/employee/asset-discount-main.jsp");
	}

	private List<AgreementAsset> getAsset(Connection conn, Agreement agreement) {
		BSBeanUtils bu = new BSBeanUtils();
		List<AgreementAsset> out = (List<AgreementAsset>) bu.list(conn, new AgreementAsset(), "cAgreement=?",
				agreement.getId());
		return out;
	}

	private List<AgreementDiscount> getDiscount(Connection conn, Agreement agreement) {
		BSBeanUtils bu = new BSBeanUtils();
		List<AgreementDiscount> out = (List<AgreementDiscount>) bu.list(conn, new AgreementDiscount(), "cAgreement=?",
				agreement.getId());
		return out;
	}

	private Agreement getAgreement(Connection conn, Employee employee) {
		AgreementService as = new AgreementServiceImpl();
		Agreement agreement = as.getAgreementByEmployee(conn, employee.getId());

		return agreement;
	}

	/**
	 * <code>
		private List<Object> getParams(Period period, Employee employee) {
			List<Object> out = new ArrayList<Object>();
			out.add(period.getId());
			out.add(employee.getId());
			return out;
		}
	
		private Period getPeriod(Connection conn) {
			PeriodService ps = new PeriodServiceImpl();
			Period period = ps.getOpenedPeriod(conn);
			return period;
		}
	</code>
	 */
	private Employee getEmployee(Connection conn, HttpServletRequest request) {
		Employee employee = null;
		String idAsParameter = request.getParameter("cId");
		String employeeId = idAsParameter == null ? request.getAttribute("cId").toString() : idAsParameter;
		Long id = Long.parseLong(employeeId);
		EmployeeService employeeService = new EmployeeServiceImpl();
		employee = employeeService.getEmployee(conn, id);
		return employee;
	}
}
