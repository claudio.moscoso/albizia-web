package cl.buildersoft.albizia.web.servlet;

import cl.buildersoft.framework.web.servlet.BSHttpServlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestDataTable
 */

@WebServlet("/servlet/test/DataTable")
public class TestDataTable extends BSHttpServlet {
	private static final long serialVersionUID = 6974467039136440557L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestDataTable() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.forward(request, response, "/WEB-INF/jsp/test/main.jsp");
	}

}
