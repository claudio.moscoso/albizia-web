package cl.buildersoft.albizia.business.dto;

import java.math.BigDecimal;

public class EmployeeHoursWorkedDTO {
	Long id = null;
	Long employeeId = null;
	String rut = null;
	String name = null;
	BigDecimal amount = null;

	public EmployeeHoursWorkedDTO() {
	}

	public EmployeeHoursWorkedDTO(Long id, Long employeeId, String rut, String name, BigDecimal amount) {
		super();
		this.id = id;
		this.employeeId = employeeId;
		this.rut = rut;
		this.name = name;
		this.amount = amount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	@Override
	public String toString() {
		return "EmployeeHoursWorkedDTO [id=" + id + ", employeeId=" + employeeId + ", rut=" + rut + ", name=" + name
				+ ", amount=" + amount + "]";
	}

}
