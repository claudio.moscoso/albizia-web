package cl.buildersoft.albizia.business.dto;

import java.math.BigDecimal;

public class EmployeeListDTO {
	private Long id = null;
	private String rut = null;
	private String name = null;
	private BigDecimal baseSalary = null;
	private Integer overtimeTime = null;
	private BigDecimal liquidSalary = null;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getBaseSalary() {
		return baseSalary;
	}
	public void setBaseSalary(BigDecimal baseSalary) {
		this.baseSalary = baseSalary;
	}
	public Integer getOvertimeTime() {
		return overtimeTime;
	}
	public void setOvertimeTime(Integer overtimeTime) {
		this.overtimeTime = overtimeTime;
	}
	public BigDecimal getLiquidSalary() {
		return liquidSalary;
	}
	public void setLiquidSalary(BigDecimal liquidSalary) {
		this.liquidSalary = liquidSalary;
	}

}
