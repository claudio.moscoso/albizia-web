package cl.buildersoft.albizia.business.beans;

import java.math.BigDecimal;

import cl.buildersoft.dalea.type.DiscountType;
import cl.buildersoft.framework.beans.BSBean;
import cl.buildersoft.framework.type.Currency;

public class AgreementDiscount extends BSBean {
	private static final long serialVersionUID = -5544086631797608028L;
	private String TABLE = "tAgreementDiscount";
	private Long agreement = null;
	private String key = null;
	private DiscountType discountType = null;
	private BigDecimal amount = null;
	private Currency currency = null;
	private String label = null;

	public Long getAgreement() {
		return agreement;
	}

	public void setAgreement(Long agreement) {
		this.agreement = agreement;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public DiscountType getDiscountType() {
		return discountType;
	}

	public void setDiscountType(DiscountType discountType) {
		this.discountType = discountType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
