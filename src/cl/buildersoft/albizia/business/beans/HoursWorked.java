package cl.buildersoft.albizia.business.beans;

import java.math.BigDecimal;

import cl.buildersoft.framework.beans.BSBean;

public class HoursWorked extends BSBean {
	private static final long serialVersionUID = 942026405046447443L;
	private String TABLE = "tHoursWorked";
	private Long period = null;
	private Long employee = null;
	private BigDecimal amount = null;

	public Long getPeriod() {
		return period;
	}

	public void setPeriod(Long period) {
		this.period = period;
	}

	public Long getEmployee() {
		return employee;
	}

	public void setEmployee(Long employee) {
		this.employee = employee;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "HoursWorked [Id=" + getId() + ", period=" + period + ", employee=" + employee + ", amount=" + amount
				+ "]";
	}

}
