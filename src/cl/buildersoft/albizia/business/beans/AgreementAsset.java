package cl.buildersoft.albizia.business.beans;

import java.math.BigDecimal;

import cl.buildersoft.dalea.type.AssetType;
import cl.buildersoft.framework.beans.BSBean;
import cl.buildersoft.framework.type.Currency;

public class AgreementAsset extends BSBean {
	private static final long serialVersionUID = 5837144782171562899L;
	@SuppressWarnings("unused")
	private String TABLE = "tAgreementAsset";
	/**
	 * <code>
	| cId        | bigint(20)                  | NO   | PRI | NULL    | auto_increment |
	| cAgreement | bigint(20)                  | NO   | MUL | NULL    |                |
	| cKey       | varchar(10)                 | NO   |     | NULL    |                |
	| cAssetType | enum('ASSIGNMENT','NORMAL') | NO   |     | NORMAL  |                |
	| cAmount    | decimal(10,4)               | NO   |     | 0.0000  |                |
	| cCurrency  | enum('CLP','CLF','PERCENT') | NO   |     | CLP     |                |
	| cLabel     | varchar(100)                | YES  |     | NULL    |                |
	   </code>
	 */

	private Long agreement = null;
	private String key = null;
	private AssetType assetType = null;
	private BigDecimal amount = null;
	private Currency currency = null;
	private String label = null;

	public Long getAgreement() {
		return agreement;
	}

	public void setAgreement(Long agreement) {
		this.agreement = agreement;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public AssetType getAssetType() {
		return assetType;
	}

	public void setAssetType(AssetType assetType) {
		this.assetType = assetType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
