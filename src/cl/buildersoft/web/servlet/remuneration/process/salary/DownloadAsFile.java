package cl.buildersoft.web.servlet.remuneration.process.salary;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cl.buildersoft.albizia.service.PeriodService;
import cl.buildersoft.albizia.service.impl.PeriodServiceImpl;
import cl.buildersoft.dalea.bean.Period;
import cl.buildersoft.framework.database.BSmySQL;
import cl.buildersoft.framework.exception.BSDataBaseException;
import cl.buildersoft.framework.util.BSConfig;
import cl.buildersoft.framework.util.BSConnectionFactory;
import cl.buildersoft.framework.util.BSDateTimeUtil;
import cl.buildersoft.framework.web.servlet.BSHttpServlet;



@WebServlet("/servlet/remuneration/process/salary/DownloadAsFile")
/**
 * Esta clase se puede mejorar para poder hacerla generica y que otras extiandan de ésta para exportar a csv cualquier resultado de un SP*/
public class DownloadAsFile extends BSHttpServlet {
	private static final Logger LOG = LogManager.getLogger(DownloadAsFile.class);
	private static final long serialVersionUID = -4993597280195709037L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BSConnectionFactory cf = new BSConnectionFactory();
		Connection conn = cf.getConnection(request);
		PeriodService ps = new PeriodServiceImpl();
		Period period = ps.getOpenedPeriod(conn);

		BSmySQL mysql = new BSmySQL();
		ResultSet rs = mysql.callSingleSP(conn, "pListBook", null);

		ServletOutputStream output = configHeaderAsCSV(response, "libro-remuneraciones-" + ps.periodAsShortString(period));
		CsvWriter csv = new CsvWriter(output, new BSConfig().getCSVSeparator(conn), Charset.defaultCharset());

		try {
			Integer index = 1;
			String type = null;
			String value = null;
			ResultSetMetaData metaData = rs.getMetaData();
			for (index = 1; index <= metaData.getColumnCount(); index++) {
				csv.write(metaData.getColumnLabel(index));
			}
			csv.endRecord();

			while (rs.next()) {
				for (index = 1; index <= metaData.getColumnCount(); index++) {
					type = metaData.getColumnTypeName(index);
					value = formatData(conn, rs.getString(index), type);
					csv.write(value);
				}
				csv.endRecord();
			}
			csv.flush();
			csv.close();

		} catch (SQLException e) {
			throw new BSDataBaseException(e);
		} finally {
			mysql.closeSQL(rs);
			mysql.closeConnection(conn);
		}
	}

	private String formatData(Connection conn, String data, String type) {
		LOG.entry(conn.toString(), data, type);
		String out = "";
		String format = null;

		if (data == null) {
			out = "";
		} else {
			if ("date".equalsIgnoreCase(type)) {
				Calendar cal = BSDateTimeUtil.string2Calendar(data, "yyyy-MM-dd");
				format = BSDateTimeUtil.getFormatDate(conn);
				out = BSDateTimeUtil.calendar2String(cal, format);
			} else if ("double".equalsIgnoreCase(type)) {
				format = "########0.00";
				Double dataDouble = Double.parseDouble(data);
				out = number2String(dataDouble, format);
			} else if ("int".equalsIgnoreCase(type)) {
				format = "########0";
				Integer dataInteger = Integer.parseInt(data);
				out = number2String(dataInteger, format);
			} else if ("bit".equalsIgnoreCase(type)) {
				if ("1".equals(data)) {
					out = "Si";
				} else {
					out = "No";
				}
			} else {
				out = data;
			}
		}
		LOG.exit(out);
		return out;
	}

	private String number2String(Object value, String format) {
		String out = "";
		if (value != null) {
			Format formatter = new DecimalFormat(format);
			out = formatter.format(value);
		}
		return out;
	}
}
